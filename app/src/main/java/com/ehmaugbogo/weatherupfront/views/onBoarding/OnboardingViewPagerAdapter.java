package com.ehmaugbogo.weatherupfront.views.onBoarding;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ehmaugbogo.weatherupfront.R;

import java.util.List;

public class OnboardingViewPagerAdapter extends PagerAdapter {

    Context mContext ;
    List<OnboardingScreenItem> mListScreen;

    public OnboardingViewPagerAdapter(Context mContext, List<OnboardingScreenItem> mListScreen) {
        this.mContext = mContext;
        this.mListScreen = mListScreen;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View onboardingLayout = inflater.inflate(R.layout.onboarding_layout, null);

        ImageView imgSlide = onboardingLayout.findViewById(R.id.img_onboarding);
        TextView description = onboardingLayout.findViewById(R.id.onb_text);

        description.setText(mListScreen.get(position).getDescription());
        imgSlide.setImageResource(mListScreen.get(position).getScreenImg());

        container.addView(onboardingLayout);

        return onboardingLayout;

    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
