package com.ehmaugbogo.weatherupfront.api;

import com.ehmaugbogo.weatherupfront.api.Response.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface WeatherAPI {

    @GET("weather")
    Call<WeatherResponse> fetchWeatherByGeoCord(@Query("lat") double lat, @Query("lon") double lon , @Query("appid") String api_key);


    //    Uneeded API Endpoints :-)

    /*
    @GET("/weather")
    Call<User> weatherCallByZipCode(@Query("zip") String zip_code_and_country_code , @Header("appid") String api_key);

    @GET("/find")
    Call<User> weatherCallForCitiesInCycle(@Query("lat") int lat, @Query("lon") int lon ,@Query("cnt") int cnt , @Header("appid") String api_key);

    @GET("/group")
    Call<User> weatherCallByForSeveralCities(@Query("id") String id_for_several_cities , @Header("appid") String api_key);

    @GET("/weather")
    Call<User> weatherCallByCityName(@Query("q") String city_name, @Header("appid") String api_key);

    @GET("/weather")
    Call<User> weatherCallByCityNameAndCountryCode(@Query("q") String city_name_and_country_code , @Header("appid") String api_key);

    @GET("/weather")
    Call<User> weatherCallByCityId(@Query("id") int city_id , @Header("appid") String api_key);

    **/
}
