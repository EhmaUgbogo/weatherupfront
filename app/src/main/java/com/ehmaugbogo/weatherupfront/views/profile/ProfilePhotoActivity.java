package com.ehmaugbogo.weatherupfront.views.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;

import static com.ehmaugbogo.weatherupfront.views.profile.ProfileActivity.setWindowFlag;

public class ProfilePhotoActivity extends BaseActivity {
    public static final String PROFILE_USER = "com.ehmaugbogo.weatherupfront.PROFILE_USER";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_photo);

        setWindowFlag(this);
        ImageView mProfileImage = findViewById(R.id.profile_photo_display_image);
        ProgressBar mProgressBar = findViewById(R.id.profile_photo_display_progress);
        mProgressBar.setVisibility(View.VISIBLE);

        String imageUrl = getIntent().getStringExtra(PROFILE_USER);
        Glide.with(this).load(imageUrl).into(mProfileImage);
        mProgressBar.setVisibility(View.GONE);

    }

}
