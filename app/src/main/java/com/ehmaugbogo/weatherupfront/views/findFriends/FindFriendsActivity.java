package com.ehmaugbogo.weatherupfront.views.findFriends;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackRequest;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.ehmaugbogo.weatherupfront.views.register.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_TRACK_REQUEST;

public class FindFriendsActivity extends BaseActivity {
    private static final String TAG = "FindFriendsActivity";
    private EditText requestEmailEditText;
    private Button sentBtn;
    private ProgressBar progressBar;

    private String requestEmail;
    private User appUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        setUpViews();

        appUser = App.getAppUser();

    }

    private void setUpViews() {
        progressBar = findViewById(R.id.find_friend_progress);
        requestEmailEditText = findViewById(R.id.txt_ff_email);
        sentBtn = findViewById(R.id.btn_send_request);

        sentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestEmail = requestEmailEditText.getText().toString().trim();
                if(checkNetworkState()&&validateEmailInput()){
                    findFriendByEmail();
                }
            }
        });
    }


    private void findFriendByEmail() {
        if(requestEmail.equals(appUser.getEmail())){
            showToast("You cannot send tracking request to yourself");
            return;
        }
        showProgress();

        userRef.whereEqualTo("email",requestEmail).limit(1).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()&&task.getResult()!=null){
                            if(!task.getResult().isEmpty()){
                                DocumentSnapshot documentSnapshot = task.getResult().getDocuments().get(0);
                                User user = documentSnapshot.toObject(User.class);
                                user.setUid(documentSnapshot.getId());
                                checkIfRequestSentBefore(user);
                            } else {
                                requestEmailEditText.setError("No user with email: "+requestEmail);
                                Log.w(TAG, "findFriendByEmail: No user with email - "+requestEmail);
                                hideProgress();
                            }
                            Log.w(TAG, "onComplete: findFriendByEmail");
                        } else {
                            requestEmailEditText.setError("No user with email: "+task.getException().getMessage());
                            Log.w(TAG, "findFriendByEmail: No user with email - "+task.getException().getMessage());
                            hideProgress();
                        }
                    }
                });
    }


    private void checkIfRequestSentBefore(User trackedUser) {
        requestRef.whereEqualTo("sender.uid",appUser.getUid())
                .whereEqualTo("sentToUser.uid",trackedUser.getUid())
                .limit(1).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()&&task.getResult()!=null){
                    if(!task.getResult().isEmpty()){
                        showToast("You've already sent a request to this user before");
                        hideProgress();
                    } else {
                        sentRequestToUser(trackedUser);
                    }
                    Log.w(TAG, "onComplete: checkIfRequestSentBefore");
                } else {
                    requestEmailEditText.setError("Check Failed: "+task.getException().getMessage());
                    Log.w(TAG, "checkIfRequestSentBefore: Failed - "+task.getException().getMessage());
                    hideProgress();
                }
            }
        });


    }

    private void sentRequestToUser(User trackedUser) {
        TrackRequest trackRequest = new TrackRequest(App.getAppUser(), trackedUser, null);

        requestRef.document().set(trackRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    showAlert();
                    Log.w(TAG, "findFriendByEmail: track request sent successfully");
                } else {
                    Log.w(TAG, "findFriendByEmail: Could not send track request");
                    Log.w(TAG, "findFriendByEmail: Resending");
                    sentRequestToUser(trackedUser);
                }
            }
        });

    }


    private boolean validateEmailInput() {

        if (requestEmail.isEmpty()) {
            showToast("Input email of a user you intend to track");
            return false;
        }

        if (!RegisterActivity.isValidEmail(requestEmail)) {
            showToast("Not a valid email");
            return false;
        }

        return true;
    }


    public void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
        sentBtn.setEnabled(false);
    }

    public void hideProgress(){
        progressBar.setVisibility(View.INVISIBLE);
        sentBtn.setEnabled(true);

    }
    public void showAlert() {
        final AlertDialog.Builder myDialog = new AlertDialog.Builder(this);
        final View customView = getLayoutInflater().inflate(R.layout.activity_request_sent, null);
        Button continueBtn = customView.findViewById(R.id.btn_rs_continue);
        myDialog.setView(customView);
        final AlertDialog dialog = myDialog.create();
        dialog.show();
        hideProgress();
        dialog.setCanceledOnTouchOutside(false);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestEmailEditText.getText().clear();
                dialog.dismiss();
            }
        });


    }

}
