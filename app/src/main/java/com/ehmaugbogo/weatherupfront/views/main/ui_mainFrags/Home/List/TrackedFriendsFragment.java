package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.List;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel.WeatherViewModel;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.views.main.HomeBaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.adapters.TrackedFriendsAdapter;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class TrackedFriendsFragment extends HomeBaseFragment {
    private static final String TAG = "TrackedFriendsFragment";
    private View nothingFoundView;
    private ListenerRegistration listenerRegistration;
    private TrackedFriendsAdapter trackedFriendsAdapter;
    private WeatherViewModel viewModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tracked_friends, container, false);
        setup(view);
        return view;
    }

    private void setup(View view) {
        viewModel = ViewModelProviders.of(getActivity()).get(WeatherViewModel.class);

        RecyclerView recyclerView = view.findViewById(R.id.friend_recycler);
        nothingFoundView = view.findViewById(R.id.nothing_found);
        trackedFriendsAdapter = new TrackedFriendsAdapter(getActivity(),viewModel);
        recyclerView.setAdapter(trackedFriendsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }


    @Override
    public void onStart() {
        super.onStart();
        getTrackedFriends();
    }

    private void getTrackedFriends() {
        listenerRegistration = userRef.whereArrayContains("trackedBy", appUser().getUid())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "onEvent: " + e.getMessage());
                            return;
                        }

                        List<User> trackFriendList = new ArrayList<>();

                        if (queryDocumentSnapshots != null) {
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                User trackFriend = documentSnapshot.toObject(User.class);
                                trackFriend.setUid(documentSnapshot.getId());
                                trackFriendList.add(trackFriend);
                            }
                        }

                        decideDisplayView(trackFriendList);

                        trackedFriendsAdapter.summitTrackFriends(trackFriendList);
                        Log.d(TAG, "onEvent: trackFriendList " + trackFriendList.size());
                    }
                });
    }

    private void decideDisplayView(List<User> trackFriendList) {
        if (trackFriendList.size() > 0){
            hideEmptyView();
        } else {
            showEmptyView();
        }
    }

    private void showEmptyView() {
        nothingFoundView.setVisibility(View.VISIBLE);
    }

    private void hideEmptyView() {
        nothingFoundView.setVisibility(View.GONE);
    }


    @Override
    public void onStop() {
        super.onStop();
        listenerRegistration.remove();
    }
}
