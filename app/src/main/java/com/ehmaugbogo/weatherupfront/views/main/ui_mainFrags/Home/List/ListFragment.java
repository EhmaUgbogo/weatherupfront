package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.List;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.main.HomeBaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.MainActivity;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.ActiveHomeFragmentState;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;


public class ListFragment extends HomeBaseFragment {
    private ViewPager viewPager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootActivity().activeHomeFragmentState = ActiveHomeFragmentState.LIST;


        View view = inflater.inflate(R.layout.fragment_list, container, false);
        viewPager = view.findViewById(R.id.list_viewpager);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT ,  getContext() );
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabLayout = view.findViewById(R.id.list_tab);
        tabLayout.setupWithViewPager(viewPager);

        listener();
        
        viewPager.setCurrentItem(MainActivity.LAST_LIST_VIEWPAGER_POSITION,true);

        return view;
    }

    private void listener() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                MainActivity.LAST_LIST_VIEWPAGER_POSITION=position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
