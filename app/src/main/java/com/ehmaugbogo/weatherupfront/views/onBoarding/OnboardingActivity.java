package com.ehmaugbogo.weatherupfront.views.onBoarding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.login.LoginActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class OnboardingActivity extends AppCompatActivity {

    private ViewPager screenPager;
    OnboardingViewPagerAdapter onboardingViewPagerAdapter;
    TabLayout tabIndicator;
    Button btnNext;
    int position = 0;
    Button btnDiveIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        tabIndicator = findViewById(R.id.tab_indicator);
        btnNext = findViewById(R.id.btn_next_onb);
        btnDiveIn = findViewById(R.id.btn_dive_in);

        // fill list screen
        final List<OnboardingScreenItem> mList = new ArrayList<>();
        mList.add(new OnboardingScreenItem( "Track Weather Condition Of Where You Are Headed",R.drawable.onboard1));
        mList.add(new OnboardingScreenItem( "Search Through Maps and Know Weather Instantly",R.drawable.onboard2));
        mList.add(new OnboardingScreenItem( "Track Friends and Know Their Various Weather Conditions",R.drawable.onboard3));

        // viewpager setup
        screenPager =findViewById(R.id.onb_viewpager);
        onboardingViewPagerAdapter = new OnboardingViewPagerAdapter(this, mList);
        screenPager.setAdapter(onboardingViewPagerAdapter);

        // tablayout with viewpager
        tabIndicator.setupWithViewPager(screenPager);

        // next button click Listener
        btnNext.setOnClickListener(view -> {
            position = screenPager.getCurrentItem();
            if (position < mList.size()) {
                position++;
                screenPager.setCurrentItem(position);
            }

            if (position == mList.size()-1) { // when we get to the last screen

                // TODO : show the Dive Right In Button and hide the next button
                loadLastScreen();
            }

        });

        // tablayout change listener
        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == mList.size()-1) {

                    loadLastScreen();
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // Dive Right in button click listener
        btnDiveIn.setOnClickListener(view -> {
            //open main activity
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        });
    }

    // show the Dive Right In Button and hide the next button
    private void loadLastScreen() {
        btnNext.setVisibility(View.INVISIBLE);
        btnDiveIn.setVisibility(View.VISIBLE);
    }
}
