package com.ehmaugbogo.weatherupfront.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@IgnoreExtraProperties
public class User implements Parcelable {
    private String uid;
    private String firstName;
    private String lastName;
    private String displayName;
    private String email;
    private String phoneNo;
    private String imageUrl;
    private String address;
    private @ServerTimestamp Date dateJoined; //Be careful of this property, It should not be changed after reg
    private List<String> trackedBy =new ArrayList<>();
    private List<String> tracking =new ArrayList<>();
    private GeoPoint location;
    private @ServerTimestamp Date lastActive; //This will reflect every change

    //private List<User> tracking =new ArrayList<>();

    public User() {
        //Needed
    }



    public User(String displayName, String email) {
        this.displayName = displayName;
        this.email = email;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public List<String> getTrackedBy() {
        return trackedBy;
    }

    public void setTrackedBy(List<String> trackedBy) {
        this.trackedBy = trackedBy;
    }

    public List<String> getTracking() {
        return tracking;
    }

    public void setTracking(List<String> tracking) {
        this.tracking = tracking;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public Date getLastActive() {
        return lastActive;
    }

    public void setLastActive(Date lastActive) {
        this.lastActive = lastActive;
    }


    protected User(Parcel in) {
        uid = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        displayName = in.readString();
        email = in.readString();
        phoneNo = in.readString();
        imageUrl = in.readString();
        address = in.readString();
        trackedBy = in.createStringArrayList();
        tracking = in.createStringArrayList();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(displayName);
        parcel.writeString(email);
        parcel.writeString(phoneNo);
        parcel.writeString(imageUrl);
        parcel.writeString(address);
        parcel.writeStringList(trackedBy);
        parcel.writeStringList(tracking);
    }
}
