package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel.WeatherViewModel;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackRequest;
import com.ehmaugbogo.weatherupfront.views.BaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.OnRequestClickedCallBack;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.adapter.ReceivedRequestAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class ReceivedRequestFragment extends BaseFragment implements OnRequestClickedCallBack {

    private static final String TAG = "ReceivedRequestFragment";
    private WeatherViewModel viewModel;
    private ReceivedRequestAdapter receivedRequestAdapter;
    private ListenerRegistration listenerRegistration;
    private AlertDialog alertDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_received_request, container, false);

        setUp(root);

        viewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        observeRequest();

        return root;
    }


    private void setUp(View root) {
        RecyclerView recyclerView = root.findViewById(R.id.received_recycler);
        receivedRequestAdapter = new ReceivedRequestAdapter(getContext());
        receivedRequestAdapter.setOnRequestClicked(this);
        recyclerView.setAdapter(receivedRequestAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    private void observeRequest() {
        viewModel.getReceivedRequestData().observe(this, new Observer<List<TrackRequest>>() {
            @Override
            public void onChanged(List<TrackRequest> receivedTrackRequests) {
                receivedRequestAdapter.summitTrackRequests(receivedTrackRequests);
                Log.d(TAG, "observeRequest: receivedTrackRequests " + receivedTrackRequests.size());
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: Called");
        getReceivedRequest();

    }

    private void getReceivedRequest() {
        listenerRegistration = requestRef.whereEqualTo("sentToUser.uid", appUser().getUid())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "onEvent: " + e.getMessage());
                            return;
                        }

                        List<TrackRequest> trackRequestList = new ArrayList<>();


                        if (queryDocumentSnapshots != null) {
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                TrackRequest trackRequest = documentSnapshot.toObject(TrackRequest.class);
                                trackRequest.setId(documentSnapshot.getId());
                                trackRequestList.add(trackRequest);
                            }
                        }

                        receivedRequestAdapter.summitTrackRequests(trackRequestList);
                        Log.d(TAG, "onEvent: trackReceivedRequestList " + trackRequestList.size());
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        listenerRegistration.remove();
    }

    @Override
    public void onRequestClicked(TrackRequest trackRequest) {
        openRequestClickedDialog(trackRequest);
    }

    @Override
    public void onRequestDeleteBtnClicked(TrackRequest trackRequest) {
        //Do nothing... Broke a rule here
    }


    private void openRequestClickedDialog(TrackRequest trackRequest) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final View customView = getLayoutInflater().inflate(R.layout.dialog_tracked_request, null);

        ImageButton confirmBtn = customView.findViewById(R.id.confirm_btn);
        ImageButton cancelBtn = customView.findViewById(R.id.cancel_btn);
        TextView message = customView.findViewById(R.id.txt_mssge);
        message.setText(String.format("%s is requesting to track you and know your weather status.",
                trackRequest.getSender().getDisplayName()));

        builder.setView(customView);
        alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptTrackRequest(trackRequest);
                alertDialog.dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*deleteRequestFromDatabase(trackRequest);
                alertDialog.dismiss();*/
                openCancleRequestDialog(trackRequest);
            }
        });


    }

    private void openCancleRequestDialog(TrackRequest trackRequest) {
        AlertDialog deleteDialog=new AlertDialog.Builder(getActivity())
                .setMessage("Are you sure you want to delete the request from "+trackRequest.getSender().getDisplayName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteRequestFromDatabase(trackRequest);
                    }
                })
                .setNegativeButton("No",null)
                .setNeutralButton("No Leave it for now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        alertDialog.dismiss();
                    }
                }).create();
        deleteDialog.show();
    }


    //TODO:
    private void acceptTrackRequest(TrackRequest trackRequest) {

        userRef.document(appUser().getUid())
                .update("trackedBy", FieldValue.arrayUnion(trackRequest.getSender().getUid()))
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "acceptTrackRequest: AppUser accepted Track request");
                            showToast("Track request accepted");

                            deleteRequestFromDatabase(trackRequest);
                            updateSenderTrackingList(trackRequest);
                        } else {
                            Log.d(TAG, "acceptTrackRequest: Failed Retrying");
                            acceptTrackRequest(trackRequest);
                        }
                    }
                });

    }

    private void updateSenderTrackingList(TrackRequest trackRequest) {
        userRef.document(trackRequest.getSender().getUid())
                .update("tracking", FieldValue.arrayUnion(appUser().getUid()))
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "updateSenderTrackingList: Sender can now track this app user");
                        } else {
                            Log.d(TAG, "updateSenderTrackingList: Failed Retrying");
                            updateSenderTrackingList(trackRequest);
                        }
                    }
                });

    }

    private void deleteRequestFromDatabase(TrackRequest trackRequest) {
        requestRef.document(trackRequest.getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "onComplete: deleteTrackRequestFromDatabase Successful");
                    Log.d(TAG, "Deleting Tracking Request after acceptance");
                } else {
                    Log.d(TAG, "onComplete: deleteTrackRequestFromDatabase Failed");
                    Log.d(TAG, "Retrying");
                    deleteRequestFromDatabase(trackRequest);
                }
            }
        });
    }


}
