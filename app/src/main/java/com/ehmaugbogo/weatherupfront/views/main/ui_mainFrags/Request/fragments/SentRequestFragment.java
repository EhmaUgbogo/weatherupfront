package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackRequest;
import com.ehmaugbogo.weatherupfront.views.BaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.OnRequestClickedCallBack;
import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel.WeatherViewModel;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.adapter.SentRequestAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class SentRequestFragment extends BaseFragment implements OnRequestClickedCallBack {
    private static final String TAG = "SentRequestFragment";
    private WeatherViewModel viewModel;
    private SentRequestAdapter sentRequestAdapter;
    private ListenerRegistration listenerRegistration;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sent_request, container, false);

        setUp(root);

        viewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        observeRequest();

        return root;
    }


    private void setUp(View root) {
        RecyclerView recyclerView = root.findViewById(R.id.sent_recycler);
        sentRequestAdapter = new SentRequestAdapter(getContext());
        sentRequestAdapter.setOnRequestClicked(this);
        recyclerView.setAdapter(sentRequestAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    private void observeRequest() {
        viewModel.getSentRequestData().observe(this, new Observer<List<TrackRequest>>() {
            @Override
            public void onChanged(List<TrackRequest> sentTrackRequests) {
                sentRequestAdapter.summitTrackRequests(sentTrackRequests);
                Log.d(TAG, "observeRequest: sentTrackRequests "+sentTrackRequests.size());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: Called");
        getSentRequest();

    }

    private void getSentRequest() {
        listenerRegistration = requestRef.whereEqualTo("sender.uid", appUser().getUid())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "onEvent: " + e.getMessage());
                            return;
                        }

                        List<TrackRequest> trackRequestList = new ArrayList<>();


                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            TrackRequest trackRequest = documentSnapshot.toObject(TrackRequest.class);
                            trackRequest.setId(documentSnapshot.getId());
                            trackRequestList.add(trackRequest);
                        }

                        sentRequestAdapter.summitTrackRequests(trackRequestList);
                        Log.d(TAG, "onEvent: trackSentRequestList " + trackRequestList.size());
                    }
                });
    }


    @Override
    public void onStop() {
        super.onStop();
        listenerRegistration.remove();
    }


    @Override
    public void onRequestClicked(TrackRequest trackRequest) {
        showToast("Track request will disappear when "+trackRequest.getSentToUser().getDisplayName()+" accept it");
    }

    @Override
    public void onRequestDeleteBtnClicked(TrackRequest trackRequest) {
        openDeleteDialog(trackRequest);
    }

    private void openDeleteDialog(TrackRequest trackRequest) {
        AlertDialog alertDialog=new AlertDialog.Builder(getActivity())
                .setMessage("You sent a track request to "+trackRequest.getSentToUser().getDisplayName()
                        + " Do you want to cancel it")
                .setPositiveButton("No",null)
                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteRequestFromDatabase(trackRequest);
                    }
                }).create();
        alertDialog.show();
    }


    private void deleteRequestFromDatabase(TrackRequest trackRequest) {
        requestRef.document(trackRequest.getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "onComplete: deleteTrackRequestFromDatabase Successful");
                    showToast("Previous Request sent to "+trackRequest.getSentToUser().getDisplayName()+" has been deleted");
                } else {
                    Log.d(TAG, "onComplete: deleteTrackRequestFromDatabase Failed");
                    Log.d(TAG, "Retrying");
                    deleteRequestFromDatabase(trackRequest);
                }
            }
        });
    }
}
