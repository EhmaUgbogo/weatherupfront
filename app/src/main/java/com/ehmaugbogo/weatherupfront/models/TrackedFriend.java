package com.ehmaugbogo.weatherupfront.models;

import com.google.android.gms.maps.model.LatLng;

public class TrackedFriend {
    private int friendId;
    private String name;
    private String address;
    private String imageUrl;
    private LatLng latLng;
    private String currentTemp;


    public TrackedFriend(int friendId, String name, String address, String imageUrl, LatLng latLng, String currentTemp) {
        this.friendId = friendId;
        this.name = name;
        this.address = address;
        this.imageUrl = imageUrl;
        this.latLng = latLng;
        this.currentTemp = currentTemp;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(String currentTemp) {
        this.currentTemp = currentTemp;
    }

}
