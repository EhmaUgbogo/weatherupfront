package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

public class WeatherClusterRenderer extends DefaultClusterRenderer<WeatherClusterItem> {
    private Context context;

    //Cluster Item
    private IconGenerator iconGenerator;
    private final ImageView circleImageView;

    //Cluster
    private final IconGenerator clusterIconGenerator;
    private final ImageView clusterImageView;
    private final int dimension;


    public WeatherClusterRenderer(Context context, GoogleMap map, ClusterManager clusterManager) {
        super(context, map, clusterManager);
        this.context=context;

        dimension = (int) context.getResources().getDimension(R.dimen.custom_profile_image);
        int padding= (int) context.getResources().getDimension(R.dimen.custom_profile_padding);

        //Cluster Item
        circleImageView = new ImageView(context);
        circleImageView.setLayoutParams(new ViewGroup.LayoutParams(70, 70));
        circleImageView.setPadding(padding,padding,padding,padding);

        iconGenerator = new IconGenerator(context);
        iconGenerator.setContentView(circleImageView);


        //Cluster
        View multiProfile = View.inflate(context,R.layout.multi_profile, null);
        clusterImageView = multiProfile.findViewById(R.id.image);

        clusterIconGenerator = new IconGenerator(context);
        clusterIconGenerator.setContentView(multiProfile);




    }

    @Override
    protected void onBeforeClusterItemRendered(WeatherClusterItem item, MarkerOptions markerOptions) {

        String title = setClusterProperty(item);

        Bitmap bitmap = iconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap)).title(title);

    }

    private String setClusterProperty(WeatherClusterItem item) {
        String title, desc;
        if (item.getUserOrPLace() == WeatherClusterItem.UserOrPLace.USER) {
            if(item.getUser().getImageUrl()==null){
                circleImageView.setImageResource(R.drawable.head);
            } else {
                Glide.with(context).load(item.getUser().getImageUrl()).into(circleImageView);
            }

            title = item.getUser().getDisplayName();
            desc = item.getUser().getEmail();
        } else {
            circleImageView.setImageResource(R.drawable.home);
            title = item.getTrackedPlace().getCustomName();
            desc = item.getTrackedPlace().getGeoPoint().toString();
        }
        return title;
    }

    @Override
    protected void onClusterItemRendered(WeatherClusterItem clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);

        String title = setClusterProperty(clusterItem);
        Bitmap bitmap = iconGenerator.makeIcon();
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));

    }


    @Override
    protected void onBeforeClusterRendered(Cluster<WeatherClusterItem> cluster, MarkerOptions markerOptions) {
        //super.onBeforeClusterRendered(cluster, markerOptions);

        // Draw multiple people.
        // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
        List<Drawable> profilePhotos = new ArrayList<>(Math.min(4, cluster.getSize()));
        int width = dimension;
        int height = dimension;

        /*for (WeatherClusterItem p : cluster.getItems()) {
            // Draw 4 at most.
            if (profilePhotos.size() == 4) break;
            Drawable drawable = context.getResources().getDrawable(R.drawable.head);
            drawable.setBounds(0, 0, width, height);
            profilePhotos.add(drawable);
        }

        MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
        multiDrawable.setBounds(0, 0, width, height);

        clusterImageView.setImageDrawable(multiDrawable);*/

        //Delete this inprovision
        Drawable drawable = context.getResources().getDrawable(R.drawable.head);
        clusterImageView.setImageDrawable(drawable);


        Bitmap icon = clusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }



    @Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        // Always render clusters.
        return cluster.getSize() > 1;
    }


}
