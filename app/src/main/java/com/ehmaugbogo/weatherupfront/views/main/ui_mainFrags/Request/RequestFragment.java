package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.main.HomeBaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.adapter.RequestPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class RequestFragment extends HomeBaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_request, container, false);
        setUp(rootView);
        return rootView;
    }

    private void setUp(View rootView) {
        RequestPagerAdapter requestPagerAdapter = new RequestPagerAdapter(getActivity().getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        ViewPager mViewpager = rootView.findViewById(R.id.tabs_viewPager);
        TabLayout mTabLayout = rootView.findViewById(R.id.received_sent);
        mViewpager.setAdapter(requestPagerAdapter);
        mTabLayout.setupWithViewPager(mViewpager);

    }

}
