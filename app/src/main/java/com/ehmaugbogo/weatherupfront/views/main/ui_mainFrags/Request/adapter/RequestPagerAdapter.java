package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.fragments.ReceivedRequestFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.fragments.SentRequestFragment;

public class RequestPagerAdapter extends FragmentStatePagerAdapter {

    public RequestPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ReceivedRequestFragment();
            case 1:
                return new SentRequestFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Received";
            case 1:
                return "Sent";
            default:
                return null;
        }
    }
}
