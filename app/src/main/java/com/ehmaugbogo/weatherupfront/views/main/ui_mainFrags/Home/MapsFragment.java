package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel.WeatherViewModel;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackedPlace;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.views.main.HomeBaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.MainActivity;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.ActiveHomeBottomSheetState;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.ActiveHomeFragmentState;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper.WeatherClusterItem;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper.WeatherClusterRenderer;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper.WeatherInfoWindowAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;


public class MapsFragment extends HomeBaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMyLocationClickListener, GoogleMap.OnInfoWindowClickListener, View.OnClickListener,
        ClusterManager.OnClusterItemClickListener<WeatherClusterItem>, ClusterManager.OnClusterItemInfoWindowClickListener<WeatherClusterItem>, ClusterManager.OnClusterClickListener<WeatherClusterItem>, ClusterManager.OnClusterInfoWindowClickListener<WeatherClusterItem> {

    private static final String TAG = "MapsFragment";
    private static final float DEFAULT_ZOOM = 12f;

    private GoogleMap googleMap;
    private FusedLocationProviderClient locationProviderClient;

    private Location lastLocation;
    private ClusterManager<WeatherClusterItem> clusterManager;
    private WeatherClusterRenderer clusterRenderer;
    private List<WeatherClusterItem> weatherClusterItemList;


    private MapView mapView;
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    private List<ListenerRegistration> listenerRegistrations;

    private WeatherClusterItem lastClusterItem;
    private WeatherClusterItem lastShownClusterItem;


    private int zoomLevel;
    private WeatherViewModel viewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maps, container, false);
        rootActivity().activeHomeFragmentState = ActiveHomeFragmentState.MAP;


        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mapView = rootView.findViewById(R.id.map);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        init(rootView);

        return rootView;
    }


    //Step 2
    private void init(View rootView) {
        locationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        rootView.findViewById(R.id.map_zoom_out_picker).setOnClickListener(this);
        listenerRegistrations = new ArrayList<>();
        viewModel = ViewModelProviders.of(getActivity()).get(WeatherViewModel.class);

    }


    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        Log.w(TAG, "onMapReady: Called");

        updateLocationUI();
    }


    protected void updateLocationUI() {
        Log.w(TAG, "updateLocationUI: Called");
        if (googleMap == null) {
            return;
        }

        Log.w(TAG, "updateLocationUI: Called googleMap available");

        if (isPermissionGranted()) {
            Log.w(TAG, "updateLocationUI: Called locationPermissionGranted");
            initClusterFunctions();

            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(false);
            googleMap.getUiSettings().setScrollGesturesEnabled(true);
            googleMap.getUiSettings().setIndoorLevelPickerEnabled(true);
            googleMap.getUiSettings().setTiltGesturesEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(false);

            getLastKnownLocation();
        } else {
            Log.w(TAG, "updateLocationUI: Called locationPermission Not Granted");
            googleMap.setMyLocationEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            lastLocation = null;
            requestLocationPermission();
        }
    }


    private void getLastKnownLocation() {
        locationProviderClient.getLastLocation().addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    lastLocation = task.getResult();
                    moveCameraToAppUser(appUser());
                    storeUserLocation(lastLocation);
                } else {
                    showToast("Could not locate your location");
                }
            }
        });

    }

    private void listen() {
        retrieveTrackedFriendsLocation();
        //retrieveTrackedPlacesLocation();
    }


    //TODO: Note - This data is also retrieved at TrackedFriends List Fragment
    private void retrieveTrackedFriendsLocation() {
        ListenerRegistration registration = userRef.whereArrayContains("trackedBy", uid)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "onEvent: " + e.getMessage());
                            return;
                        }

                        List<User> trackFriendList = new ArrayList<>();

                        if (queryDocumentSnapshots != null) {
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                User trackFriend = documentSnapshot.toObject(User.class);
                                trackFriend.setUid(documentSnapshot.getId());
                                trackFriendList.add(trackFriend);
                            }
                        }


                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                showFriendsClusterMarkers(trackFriendList);
                            }
                        });

                        Log.d(TAG, "onEvent: trackFriendList Displayed on Map" + trackFriendList.size());
                    }
                });
        listenerRegistrations.add(registration);
    }


    private void retrieveTrackedPlacesLocation() {
        ListenerRegistration registration = placeRef.whereEqualTo("ownerId", appUser().getUid())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "onEvent: " + e.getMessage());
                            return;
                        }

                        List<TrackedPlace> trackedPlaceList = new ArrayList<>();

                        if (queryDocumentSnapshots != null) {
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                TrackedPlace trackedPlace = documentSnapshot.toObject(TrackedPlace.class);
                                trackedPlace.setId(documentSnapshot.getId());
                                trackedPlaceList.add(trackedPlace);
                            }
                        }

                        Log.d(TAG, "onEvent: trackedPlaceList " + trackedPlaceList.size());
                    }
                });
        listenerRegistrations.add(registration);

    }


    private void storeUserLocation(Location location) {
        GeoPoint currentPosition = new GeoPoint(location.getLatitude(), location.getLongitude());
        Log.d(TAG, "storeUserLocation: "+ location.toString());

        appUser().setLocation(currentPosition);

        userRef.document(uid)
                .update("location",currentPosition).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "storeUserLocation for the first time: Successful");
                } else {
                    Log.d(TAG, "storeUserLocation Failed: " + task.getException().getMessage());
                }
            }
        });
    }


    //TODO; may need to be deleted
    @Override
    public void onMyLocationClick(@NonNull Location location) {
        lastLocation = location;
        moveCameraToAppUser(appUser());
    }


    /*******************   Move Camera & Info Window  ********************/

    private void moveCameraToAppUser(User user) {
        googleMap.clear();

        Log.w(TAG, "moveCameraToAppUser: via Location Called");
        LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.setInfoWindowAdapter(new WeatherInfoWindowAdapter(getActivity(), user));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));

        LatLngBounds bounds = getLatLngBounds(latLng);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 14));
    }


    @NonNull
    private LatLngBounds getLatLngBounds(LatLng latLng) {
        double bottomBoundary = latLng.latitude - .1;
        double leftBoundary = latLng.longitude - .1;
        double topBoundary = latLng.latitude + .1;
        double rightBoundary = latLng.longitude + .1;

        return new LatLngBounds(
                new LatLng(bottomBoundary, leftBoundary),
                new LatLng(topBoundary, rightBoundary)
        );
    }


    /*******************   Cluster Item Markers  ********************/

    private void initClusterFunctions() {
        weatherClusterItemList = new ArrayList<>();

        clusterManager = new ClusterManager<WeatherClusterItem>(getActivity(), googleMap);
        clusterRenderer = new WeatherClusterRenderer(getActivity(), googleMap, clusterManager);
        clusterManager.setRenderer(clusterRenderer);

        googleMap.setOnCameraIdleListener(clusterManager);
        googleMap.setOnMarkerClickListener(clusterManager);
        googleMap.setOnInfoWindowClickListener(clusterManager.getMarkerManager());


        clusterManager.setAnimation(true);
        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterInfoWindowClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterItemInfoWindowClickListener(this);

    }


    private void showFriendsClusterMarkers(List<User> trackFriendList) {
        clusterManager.clearItems();
        weatherClusterItemList.clear();
        //googleMap.clear();

        for (User friend : trackFriendList) {
            if (friend.getLocation() == null) {
                Log.d(TAG, "showFriendsClusterMarkers: trackFriendList has friends with null location. ");
                return;
            }

            WeatherClusterItem clusterItem = new WeatherClusterItem(friend);
            clusterItem.setTagId(friend.getUid());
            weatherClusterItemList.add(clusterItem);

            //Setting Cluster Info Window
            clusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(new WeatherInfoWindowAdapter(getActivity(), friend));

            clusterManager.addItem(clusterItem);
        }

        clusterManager.cluster();
    }


    @Override
    public boolean onClusterItemClick(WeatherClusterItem clusterItem) {

        if (lastClusterItem != null && lastClusterItem.getTagId().equals(clusterItem.getUser().getUid())) {
            showCustomToast(clusterItem.getUser().getDisplayName() + " is currently here");
            zoomLevel = 16; //Repeating user
            //HideInfoWindow & Show DetailsBottomSheet
            ShowWeatherDetailsBottomSheet(clusterItem);

        } else {
            zoomLevel = 14;
            //showInfoWindow
        }

        lastClusterItem = clusterItem;
        animateCameraToUser(clusterItem, zoomLevel);
        return true;
    }

    private void ShowWeatherDetailsBottomSheet(WeatherClusterItem clusterItem) {
        if (rootActivity().bottomSheetState == ActiveHomeBottomSheetState.NONE
                ||(lastShownClusterItem!=null&&!lastShownClusterItem.getTagId().equals(clusterItem.getTagId()))) {
            lastShownClusterItem=clusterItem;
            viewModel.setClusterItem(clusterItem);
        }
    }

    private void animateCameraToUser(WeatherClusterItem clusterItem, int zoomLevel) {
        //LatLngBounds bounds = getLatLngBounds(latLng);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(clusterItem.getPosition(), zoomLevel));
    }


    @Override
    public void onClusterItemInfoWindowClick(WeatherClusterItem weatherClusterItem) {
        showToast(weatherClusterItem.getUser().getDisplayName() + " Cluster Item Info Window Clicked");
    }


    @Override
    public boolean onClusterClick(Cluster<WeatherClusterItem> cluster) {

        // Show a toast with some info when the cluster is clicked.
        String firstName = cluster.getItems().iterator().next().getUser().getDisplayName();
        showToast(cluster.getSize() + " (including " + firstName + ")");

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<WeatherClusterItem> cluster) {
        showToast("Cluster Info Window of size " + cluster.getSize() + " Clicked");
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        for (WeatherClusterItem item : weatherClusterItemList) {
            if (marker.getPosition().latitude == item.getPosition().latitude) {
                showToast(item.getTitle());
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.map_zoom_out_picker:
                mapZoomAllOut();
                break;

        }
    }

    private void mapZoomAllOut() {
        if (lastClusterItem != null && zoomLevel == 16) {
            zoomLevel = 14;
        } else if (lastClusterItem != null && zoomLevel == 14) {
            zoomLevel = 10;
        } else {
            displayAllCluster(weatherClusterItemList);
            return;
        }

        animateCameraToUser(lastClusterItem, zoomLevel);
    }

    private void displayAllCluster(List<WeatherClusterItem> clusters) {
        if (clusters.size() == 0) {
            moveCameraToAppUser(appUser());
            return;
        }

        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : clusters) {
            builder.include(item.getPosition());
        }
        final LatLngBounds bounds = builder.build();

        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*******************   Permission Methods Abstracted to another class  ********************/

    private boolean isPermissionGranted() {
        return MainActivity.locationPermissionGranted;
    }

    private void requestLocationPermission() {
        rootActivity().requestLocationPermission();
    }

    /*******************   isGpsEnabled Methods   ********************/

    private boolean isGpsEnabled() {
        return rootActivity().isGpsEnabled();
    }


    /*******************   Lifecycle Methods   ********************/


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        listen();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
        for (ListenerRegistration registration : listenerRegistrations) {
            registration.remove();
        }
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


}
