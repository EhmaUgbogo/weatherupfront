package com.ehmaugbogo.weatherupfront.models;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class TrackRequest {
    private String id;
    private User sender;
    private User sentToUser;
    private @ServerTimestamp Date sentDate;

    public TrackRequest() {

    }

    public TrackRequest(User sender, User sentToUser, Date sentDate) {
        this.sender = sender;
        this.sentToUser = sentToUser;
        this.sentDate = sentDate;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getSentToUser() {
        return sentToUser;
    }

    public void setSentToUser(User sentToUser) {
        this.sentToUser = sentToUser;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }
}
