package com.ehmaugbogo.weatherupfront.views.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.ehmaugbogo.weatherupfront.views.register.RegisterActivity;
import com.ehmaugbogo.weatherupfront.views.signup_signin.forgot_password.ForgotPasswordActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";
    private EditText  mEmail, mPassword, mConfirmPassword;
    private TextView mForgotPassword;
    //private View mRegister_layout;
    private Button mLogin;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser()!=null){
            goToMainActivity();
        }
        initView();

    }

    private void initView() {

        mEmail =  findViewById(R.id.reg_email_edt);
        mPassword = findViewById(R.id.reg_password_edt);
        mConfirmPassword = findViewById(R.id.reg_passConf_edt);
        mForgotPassword = findViewById(R.id.login_forgotPassword_tv);
        mLogin = findViewById(R.id.login_btn);
        progressBar = findViewById(R.id.login_progressBar);

        listeners();
    }

    private void listeners(){
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetworkState()) {
                    loginUser(mEmail.getText().toString(), mPassword.getText().toString());
                }
            }
        });

        mForgotPassword.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
            startActivity(intent);
        });

        ConstraintLayout mRegister_layout = findViewById(R.id.notAMember_layout);
        mRegister_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

    }

    private void loginUser(String email, String password){
        showProgress();
        if (validateInputs(email, password)){
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){

                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    Log.d(TAG, "User created successfully" + task.isSuccessful());
                    if (task.isSuccessful()){
                        goToMainActivity();
                        Toast.makeText(getApplicationContext(), "Successfully Logged In", Toast.LENGTH_LONG).show();

                    }else {
                        Toast.makeText(getApplicationContext(), "Login Failed: "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
                    hideProgress();
                }
            });
        }else {
            showToast("Check required inputs");
            hideProgress();
        }
    }


    private boolean validateInputs(String email, String password) {
        boolean status=true;

        if(!isValidEmail(email)){
            mEmail.setError("A valid Email Required");
            status =false;
        }

        if(!isValidPassword(password)){
            mPassword.setError("Password of 8 characters & above Required");
            status= false;
        }

        return status;
    }



    // validating email id
    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    // validating password with retype password
    public boolean isValidPassword(String pass) {
        return pass != null && pass.length() >= 8;
    }

    public void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
        mLogin.setEnabled(false);
    }

    public void hideProgress(){
        progressBar.setVisibility(View.INVISIBLE);
        mLogin.setEnabled(true);

    }

}
