package com.ehmaugbogo.weatherupfront.views;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.views.profile.ProfileActivity;
import com.ehmaugbogo.weatherupfront.views.profile.editProfile.EditProfileActivity;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.main.MainActivity;
import com.ehmaugbogo.weatherupfront.views.login.LoginActivity;
import com.ehmaugbogo.weatherupfront.views.register.RegisterActivity;
import com.ehmaugbogo.weatherupfront.views.signup_signin.forgot_password.ForgotPasswordActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_TRACKED_PLACES;
import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_TRACK_REQUEST;
import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_USERS;
import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_USERS_LOCATION;

public class BaseActivity extends AppCompatActivity {
    protected FirebaseFirestore db;
    protected FirebaseAuth auth;
    protected FirebaseUser firebaseUser;
    protected CollectionReference userRef;
    protected CollectionReference requestRef;
    protected CollectionReference userLocationRef;
    protected CollectionReference placeRef;
    protected StorageReference storageRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userRef = db.collection(COLLECTION_USERS);
        requestRef = db.collection(COLLECTION_TRACK_REQUEST);
        userLocationRef = db.collection(COLLECTION_USERS_LOCATION);
        placeRef = db.collection(COLLECTION_TRACKED_PLACES);
        storageRef = FirebaseStorage.getInstance().getReference("uploads");


    }

    protected void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public void goToLogin() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void goToMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    public boolean checkNetworkState() {
        if (!App.checkNetwork(this)) {
            showToast("You do not have a network connection");
            return false;
        }
        return true;
    }

    protected User appUser(){
        return App.getAppUser();
    }


}