package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags;

public enum ActiveHomeBottomSheetState {
    EXTRA_FAB_B_SHEET,
    LOCATION_B_SHEET,
    WEATHER_DETAILS_B_SHEET,
    NONE;
}
