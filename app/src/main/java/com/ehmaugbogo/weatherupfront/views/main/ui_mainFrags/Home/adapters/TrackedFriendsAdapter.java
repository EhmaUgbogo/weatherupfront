package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel.WeatherViewModel;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.api.Response.WeatherResponse;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.views.userDetails.UserDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import static com.ehmaugbogo.weatherupfront.utils.App.weatherResponses;
import static com.ehmaugbogo.weatherupfront.utils.Constants.API_KEY;

public class TrackedFriendsAdapter extends RecyclerView.Adapter<TrackedFriendsAdapter.ViewHolder> {
    private static final String TAG = "TrackedFriendsAdapter";
    private List<User> trackedFriendList=new ArrayList<>();
    private Context context;
    private WeatherViewModel viewModel;
    private Call<WeatherResponse> weatherResponseCall;



    public TrackedFriendsAdapter(Context context, WeatherViewModel viewModel) {
        this.context = context;
        this.viewModel=viewModel;
    }

    @NonNull
    @Override
    public TrackedFriendsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_details, viewGroup, false);
        return new TrackedFriendsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        User trackFriend = trackedFriendList.get(position);

        if(trackFriend.getImageUrl()==null) {
            holder.categoryIcon.setImageResource(R.drawable.ic_person_black);
        } else Glide.with(context).load(trackFriend.getImageUrl()).into(holder.categoryIcon);

        holder.name.setText(trackFriend.getDisplayName());

        fetchWeatherDetails(trackFriend,holder);

    }


    @Override
    public int getItemCount() {
        return trackedFriendList.size();
    }

    public void summitTrackFriends(List<User> trackFriendList) {
        clearWeatherResponses();
        trackedFriendList=trackFriendList;
        notifyDataSetChanged();
    }

    private void fetchWeatherDetails(User trackFriend, ViewHolder holder) {
        //cancelWeatherRequest();

        weatherResponseCall = viewModel.getWeatherAPI().fetchWeatherByGeoCord(trackFriend.getLocation().getLatitude(), trackFriend.getLocation().getLongitude(), API_KEY);
        weatherResponseCall.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()&&response.body()!=null) {
                    WeatherResponse weatherResponse = response.body();

                    weatherResponse.setStoredUser(trackFriend);
                    addWeatherResponse(weatherResponse);

                    WeatherResponse.Weather weather = weatherResponse.getWeather().get(0);


                    String weatherIconUrl= "http://openweathermap.org/img/w/" + weather.getIcon() + ".png";
                    Glide.with(context).load(weatherIconUrl).into(holder.weatherImage);

                    //weatherUserLocationAddrTv.setText(String.format("%s - %s", weatherResponse.getName(), weatherResponse.getSys().getCountry()));
                    //weatherInfoStateTv.setText(weather.getDescription());
                    holder.temprature.setText(String.valueOf(weatherResponse.getMain().getTemp() + "°F"));

                } else {
                    Log.d(TAG, "Code: " + response.code() + " message; " + response.message());
                }
                holder.weatherProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                holder.weatherProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void cancelWeatherRequest() {
        if(weatherResponseCall!=null&&weatherResponseCall.isExecuted()){
            weatherResponseCall.cancel();
        }
    }


    private void addWeatherResponse(WeatherResponse weatherResponse) {
        weatherResponses.add(weatherResponse);
    }

    private void clearWeatherResponses() {
        weatherResponses.clear();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView categoryIcon;
        private ImageView weatherImage;
        private TextView name;
        private TextView temprature;
        private  ProgressBar weatherProgressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryIcon = itemView.findViewById(R.id.item_details_weather_category_icon);
            name = itemView.findViewById(R.id.item_details_weather_name);
            temprature = itemView.findViewById(R.id.item_details_weather_temprature);
            weatherImage = itemView.findViewById(R.id.item_details_weather_img);
            weatherProgressBar = itemView.findViewById(R.id.item_details_weather_progressBar);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getAdapterPosition()!=RecyclerView.NO_POSITION){
                        displayUserDetails(getAdapterPosition());
                    }
                }
            });

        }

        private void displayUserDetails(int adapterPosition) {
            if(weatherResponses.size()==trackedFriendList.size()||weatherResponses.size()-1>=adapterPosition){
                User user = trackedFriendList.get(getAdapterPosition());
                Intent intent = new Intent(context, UserDetailsActivity.class);
                intent.putExtra(UserDetailsActivity.USER_DETAILS,user);

                context.startActivity(intent);
            } else Toast.makeText(context,"Please wait a moment",Toast.LENGTH_SHORT).show();

        }
    }
}
