package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request;

import com.ehmaugbogo.weatherupfront.models.TrackRequest;

public interface OnRequestClickedCallBack {
    void onRequestClicked(TrackRequest trackRequest);
    void onRequestDeleteBtnClicked(TrackRequest trackRequest);
}
