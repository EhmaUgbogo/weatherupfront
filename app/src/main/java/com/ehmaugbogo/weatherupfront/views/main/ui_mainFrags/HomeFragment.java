package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel.WeatherViewModel;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.api.Response.WeatherResponse;
import com.ehmaugbogo.weatherupfront.models.TrackedPlace;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.utils.Constants;
import com.ehmaugbogo.weatherupfront.views.findFriends.FindFriendsActivity;
import com.ehmaugbogo.weatherupfront.views.main.HomeBaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.List.ListFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.MapsFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper.WeatherClusterItem;
import com.ehmaugbogo.weatherupfront.views.profile.ProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.GeoPoint;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ehmaugbogo.weatherupfront.utils.Constants.API_KEY;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends HomeBaseFragment implements
        View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "HomeFragment";

    private BottomNavigationView bottomNavigationView;
    private Fragment fragment;
    private ImageButton fabImageBtn;


    private BottomSheetBehavior fabsBottomSheetBehavior,
            locationBottomSheetBehavior, userDetailsBottomSheetBehavior;

    //Search BottomSheet Views
    private EditText searchEditText, customNameEditText;
    private String searchLocation, customName;

    //WeatherUser BottomSheet Views
    private ImageView weatherUserImg;
    private TextView weatherUserNameTxtView, weatherUserLocationAddrTv, weatherInfoStateTv, weatherDegreeTv;
    private ProgressBar weatherProgressBar;
    private ImageView weatherWeatherImg;


    private WeatherViewModel viewModel;
    private Call<WeatherResponse> weatherResponseCall;
    User weatherFriend;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        bottomNavigationView = view.findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        fabImageBtn = view.findViewById(R.id.fabImageBtn);
        ImageButton addLocationBtn = view.findViewById(R.id.bottom_sheet_location_add_btn);
        ImageButton addFriendBtn = view.findViewById(R.id.bottom_sheet_friend_add_btn);

        fabImageBtn.setOnClickListener(this);
        addLocationBtn.setOnClickListener(this);
        addFriendBtn.setOnClickListener(this);


        View fab_layout = view.findViewById(R.id.bottom_sheet_extra_fab_views);
        View location_layout = view.findViewById(R.id.bottom_sheet_location_views);
        View weather_user_details_layout = view.findViewById(R.id.bottom_sh_weather_user_details);

        fabsBottomSheetBehavior = BottomSheetBehavior.from(fab_layout);
        locationBottomSheetBehavior = BottomSheetBehavior.from(location_layout);
        userDetailsBottomSheetBehavior = BottomSheetBehavior.from(weather_user_details_layout);
        bottomSheetListener();

        searchEditText = view.findViewById(R.id.bottom_sheet_location_search_editText);
        customNameEditText = view.findViewById(R.id.bottom_sheet_custom_name_editText);
        ImageButton selectLocationImageBtn = view.findViewById(R.id.bottom_sheet_location_save_imageBtn);
        selectLocationImageBtn.setOnClickListener(this);
        searchEditText.setOnClickListener(this);


        weatherUserImg = view.findViewById(R.id.bottom_sh_weather_user_image);
        weatherUserImg.setOnClickListener(this);
        weatherUserNameTxtView = view.findViewById(R.id.bottom_sh_weather_user_name);
        weatherUserLocationAddrTv = view.findViewById(R.id.bottom_sh_weather_user_location_addres);
        weatherInfoStateTv = view.findViewById(R.id.bottom_sh_weather_infoStateText);
        weatherDegreeTv = view.findViewById(R.id.bottom_sh_weather_degreeText);
        weatherProgressBar = view.findViewById(R.id.bottom_sh_weather_progressbar);
        weatherWeatherImg = view.findViewById(R.id.bottom_sh_weather_weather_image);


        viewModel = ViewModelProviders.of(getActivity()).get(WeatherViewModel.class);
        observeClusterItem();

        openMapsFragment();
    }

    private void observeClusterItem() {
        viewModel.getClusterItem().observe(getViewLifecycleOwner(), new Observer<WeatherClusterItem>() {
            @Override
            public void onChanged(WeatherClusterItem clusterItem) {
                showWeatherDetailsBottomSheet(clusterItem);
                Log.d(TAG, "onChanged: observeClusterItem Called");
            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (bottomNavigationView.getSelectedItemId() == item.getItemId()) return false;

        switch (item.getItemId()) {
            case R.id.bottom_nav_map:
                fragment = new MapsFragment();
                break;

            case R.id.bottom_nav_lists:
                fragment = new ListFragment();
                break;
        }
        setUpFragment(fragment);
        return true;
    }

    private void setUpFragment(Fragment fragment) {
        if (fragment instanceof ListFragment) {
            hideHamburgerFab();
        } else {
            showHamburgerFab();
        }

        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();
    }

    public void openMapsFragment() {
        setUpFragment(new MapsFragment());
        bottomNavigationView.setSelectedItemId(R.id.bottom_nav_map);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabImageBtn:
                fabClicked();
                break;
            case R.id.bottom_sheet_location_add_btn:
                showLocationBottomSheet();
                break;
            case R.id.bottom_sheet_friend_add_btn:
                AddNewFriend();
                break;
            case R.id.bottom_sheet_location_search_editText:
                openAutocompleteIntent();
                break;
            case R.id.bottom_sheet_location_save_imageBtn:
                saveLocation();
                break;
            case R.id.bottom_sh_weather_user_image:
                viewFiendProfile();
                break;
        }

    }


    private void openAutocompleteIntent() {
        searchEditText.getText().clear();
        rootActivity().openAutocompleteIntent();
    }


    private void AddNewFriend() {
        startActivity(new Intent(getActivity(), FindFriendsActivity.class));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideExtrafabBottomSheet();
            }
        }, 200);
    }

    //Test with Dummy data
    private void saveLocation() {
        if (!validateSearchInputs()) {
            return;
        }

        if (!customName.equals(appUser().getEmail())) return;

        GeoPoint dummyLatLng = new GeoPoint(6.465422, 3.406448);
        TrackedPlace trackedPlace = new TrackedPlace(appUser().getUid(), searchLocation, customName, dummyLatLng);
        saveLocationDetails(trackedPlace);

        showToast("Adding this location to your list of tracked places");
        hideLocationBottomSheet();
    }


    private void saveLocationDetails(TrackedPlace trackedPlace) {
        placeRef.add(trackedPlace).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "onComplete: Tracked Place added succesfully");

                } else {
                    showToast("Error: " + Objects.requireNonNull(task.getException()).getMessage());
                    Log.d(TAG, "Error: " + Objects.requireNonNull(task.getException()).getMessage());
                }
            }
        });
    }

    private boolean validateSearchInputs() {
        searchLocation = searchEditText.getText().toString().trim();
        customName = customNameEditText.getText().toString().trim();

        if (searchLocation.isEmpty()) {
            searchEditText.setError("Click to add a place");
            return false;
        }

        if (customName.isEmpty()) {
            customNameEditText.setError("Please name this place");
            return false;
        }
        return true;
    }


    //Don't change - logic mixed with listener
    private void fabClicked() {
        if (userDetailsBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            userDetailsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }

        if (locationBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            locationBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }

        if (fabsBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            fabsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else if (fabsBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            fabsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    private void bottomSheetListener() {
        fabsBottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        hideExtrafabBottomSheet();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        showExtraFabBottomSheet();
                        ;
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
        locationBottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        hideLocationBottomSheet();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        showLocationBottomSheet();
                        ;
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
        userDetailsBottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        hideWeatherDetailsBottomSheet();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        //showWeatherDetailsBottomSheet(clusterItem);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }


    private void showExtraFabBottomSheet() {
        changeFabImageToCloseIcon();
        setMainBottomSheetState(ActiveHomeBottomSheetState.EXTRA_FAB_B_SHEET);
    }

    public void hideExtrafabBottomSheet() {
        fabsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);// Don't Delete this repeat. Needed by Main
        changeFabImageToAddIcon();
        setMainBottomSheetState(ActiveHomeBottomSheetState.NONE);
    }

    private void showLocationBottomSheet() {
        //Hide ExtraBottomSheet
        fabsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        locationBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        changeFabImageToCloseIcon();
        setMainBottomSheetState(ActiveHomeBottomSheetState.LOCATION_B_SHEET);
    }

    public void hideLocationBottomSheet() {
        locationBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);// Don't Delete this repeat. Needed by Main
        changeFabImageToAddIcon();
        setMainBottomSheetState(ActiveHomeBottomSheetState.NONE);

        searchEditText.getText().clear();
        customNameEditText.getText().clear();
        searchEditText.setError(null);
        customNameEditText.setError(null);
        //rootActivity().hideSupportMap();
    }


    private void showWeatherDetailsBottomSheet(WeatherClusterItem clusterItem) {
        changeFabImageToCloseIcon();
        setMainBottomSheetState(ActiveHomeBottomSheetState.WEATHER_DETAILS_B_SHEET);
        userDetailsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        weatherProgressBar.setVisibility(View.VISIBLE);


        if(clusterItem.getUser().getImageUrl()!=null){
            Glide.with(getContext()).load(clusterItem.getUser().getImageUrl()).into(weatherUserImg);
        } else {
            weatherUserImg.setImageResource(R.drawable.head); //Default image
        }


        weatherUserNameTxtView.setText(clusterItem.getUser().getDisplayName());
        weatherUserLocationAddrTv.setText("Loading...");
        weatherInfoStateTv.setText("Loading...");
        weatherDegreeTv.setText("0°");


        if (clusterItem.getUserOrPLace() == WeatherClusterItem.UserOrPLace.USER) {
            weatherFriend=clusterItem.getUser();
        } else {
            weatherFriend=null;
        }


        fetchWeatherDetails(clusterItem);

        Log.d(TAG, "showWeatherDetailsBottomSheet: Called");

    }

    private void fetchWeatherDetails(WeatherClusterItem clusterItem) {
        cancelWeatherRequest();

        weatherResponseCall = viewModel.getWeatherAPI().fetchWeatherByGeoCord(clusterItem.getPosition().latitude, clusterItem.getPosition().longitude, API_KEY);
        weatherResponseCall.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {
                    WeatherResponse weatherResponse = response.body();
                    WeatherResponse.Weather weather = weatherResponse.getWeather().get(0);

                    String weatherIconUrl= "http://openweathermap.org/img/w/" + weather.getIcon() + ".png";
                    Glide.with(getContext()).load(weatherIconUrl).into(weatherWeatherImg);

                    weatherUserLocationAddrTv.setText(String.format("%s - %s", weatherResponse.getName(), weatherResponse.getSys().getCountry()));
                    weatherInfoStateTv.setText(weather.getDescription());
                    weatherDegreeTv.setText(String.valueOf(weatherResponse.getMain().getTemp() + "°F"));


                    Log.d(TAG, "weatherResponse: " + weatherResponse.getName() + "\nCoord: " + weatherResponse.getCoord().toString());
                    Log.d(TAG, "Code: " + response.code() + "message; " + weatherResponse.toString());
                } else {
                    Log.d(TAG, "Code: " + response.code() + " message; " + response.message());
                }
                weatherProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                weatherProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void cancelWeatherRequest() {
        if(weatherResponseCall!=null&&weatherResponseCall.isExecuted()){
            weatherResponseCall.cancel();
        }
    }


    private void viewFiendProfile() {
        if(weatherFriend!=null){
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            intent.putExtra(ProfileActivity.PROFILE_USER, weatherFriend);
            startActivity(intent);
        }
    }

    public void hideWeatherDetailsBottomSheet() {
        cancelWeatherRequest();
        changeFabImageToAddIcon();
        userDetailsBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        setMainBottomSheetState(ActiveHomeBottomSheetState.NONE);

        weatherUserNameTxtView.clearComposingText();
        weatherUserLocationAddrTv.clearComposingText();
        weatherInfoStateTv.clearComposingText();
        weatherDegreeTv.clearComposingText();

        Log.d(TAG, "hideWeatherDetailsBottomSheet: Called");
    }


    private void changeFabImageToAddIcon() {
        if (fabImageBtn.getDrawable() != getActivity().getResources().getDrawable(R.drawable.add))
            fabImageBtn.setImageResource(R.drawable.add);
    }

    private void changeFabImageToCloseIcon() {
        if (fabImageBtn.getDrawable() != getActivity().getResources().getDrawable(R.drawable.ic_close_white_24dp))
            fabImageBtn.setImageResource(R.drawable.ic_close_white_24dp);
    }


    private void hideHamburgerFab() {
        rootActivity().hideHamburgerFab();
    }

    private void showHamburgerFab() {
        rootActivity().showHamburgerFab();

    }


}
