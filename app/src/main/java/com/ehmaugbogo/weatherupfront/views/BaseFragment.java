package com.ehmaugbogo.weatherupfront.views;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.main.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_TRACKED_PLACES;
import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_TRACK_REQUEST;
import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_USERS;
import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_USERS_LOCATION;

public class BaseFragment extends Fragment {
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    protected FirebaseUser firebaseUser;
    protected CollectionReference userRef;
    protected CollectionReference requestRef;
    protected CollectionReference userLocationRef;
    protected CollectionReference placeRef;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userRef=db.collection(COLLECTION_USERS);
        requestRef = db.collection(COLLECTION_TRACK_REQUEST);
        userLocationRef = db.collection(COLLECTION_USERS_LOCATION);
        placeRef = db.collection(COLLECTION_TRACKED_PLACES);


    }

    protected void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    protected User appUser(){
        return App.getAppUser();
    }


}
