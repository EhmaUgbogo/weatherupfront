package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper;

import com.ehmaugbogo.weatherupfront.models.TrackedPlace;
import com.ehmaugbogo.weatherupfront.models.User;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class WeatherClusterItem implements ClusterItem {
    private final LatLng latLngPosition;
    private String title;
    private String snippet;
    private User user;
    private TrackedPlace trackedPlace;
    private UserOrPLace userOrPLace;

    private String tagId;


    public WeatherClusterItem(LatLng latLng, String title, String snippet, User user) {
        latLngPosition = latLng;
        this.title = title;
        this.snippet = snippet;
        this.user=user;
        userOrPLace= UserOrPLace.USER;
    }

    public WeatherClusterItem(User user) {
        latLngPosition = new LatLng(user.getLocation().getLatitude(),user.getLocation().getLongitude());
        this.title = user.getDisplayName();
        this.snippet = "This snippet is hardcoded for now";
        this.user=user;
        userOrPLace= UserOrPLace.USER;
    }

    public WeatherClusterItem(TrackedPlace trackedPlace) {
        latLngPosition = new LatLng(trackedPlace.getGeoPoint().getLatitude(), trackedPlace.getGeoPoint().getLongitude());
        this.title = trackedPlace.getCustomName();
        this.snippet = "This snippet is hardcoded for now";
        this.trackedPlace = trackedPlace;
        userOrPLace= UserOrPLace.PLACE;
    }

    @Override
    public LatLng getPosition() {
        return latLngPosition;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TrackedPlace getTrackedPlace() {
        return trackedPlace;
    }

    public void setTrackedPlace(TrackedPlace trackedPlace) {
        this.trackedPlace = trackedPlace;
    }

    public UserOrPLace getUserOrPLace() {
        return userOrPLace;
    }

    public void setUserOrPLace(UserOrPLace userOrPLace) {
        this.userOrPLace = userOrPLace;
    }


    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public enum UserOrPLace {
        PLACE,
        USER;
    }
}
