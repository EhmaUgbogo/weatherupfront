package com.ehmaugbogo.weatherupfront.views.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackedPlace;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.ActiveHomeBottomSheetState;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.ActiveHomeFragmentState;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.MapHelper;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.HomeFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.RequestFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Settings.SettingsFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;


    //For Permissions
    public static final int ERROR_DIALOG_REQUEST = 121;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 122;
    public static final int REQUEST_ENABLE_GPS = 123;

    public static final String KEY_PERMISSION_PROMPTED_AFTER_FIRST_TIME = "com.ehmaugbogo.weatherupfront_KEY_DONT_ASK_AGAIN_PERMIT_CHECKED";
    public static final int PERMISSIONS_REQUEST_NEVER_OPEN_AGAIN = 124;

    public static boolean locationPermissionGranted;

    public MapHelper mapHelper;
    private boolean isRestored;

    private CardView autocomplete_container;
    private AutocompleteSupportFragment autocompleteFragment;
    public static final int AUTOCOMPLETE_REQUEST_CODE = 652;

    private FloatingActionButton hamburger_fab;

    public ActiveHomeFragmentState activeHomeFragmentState;
    public ActiveHomeBottomSheetState bottomSheetState;
    private HomeFragment storeHomeFragment;

    public static int LAST_LIST_VIEWPAGER_POSITION;
    private TextView trackingTv;
    private TextView trackingYouTv;
    private ImageView imageView;

    private List<ListenerRegistration> listenerRegistrations;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isRestored = savedInstanceState != null;


        mapHelper = new MapHelper(this);
        listenerRegistrations = new ArrayList<>();

        begin();

    }

    public void begin() {
        if (deviceCanUseMapApi()) {
            if (locationPermissionGranted) {
                setContentView(R.layout.activity_main);
                initViews();

            } else {
                requestLocationPermission();
            }
        }
    }


    private boolean deviceCanUseMapApi() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int playServicesAvailable = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (playServicesAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (googleApiAvailability.isUserResolvableError(playServicesAvailable)) {
            Dialog errorDialog = googleApiAvailability.getErrorDialog(this, playServicesAvailable, ERROR_DIALOG_REQUEST);
            errorDialog.show();
        } else {
            showToast("Your device cannot use an app with map");
        }
        return false;
    }

    private void initViews() {
        bottomSheetState = ActiveHomeBottomSheetState.NONE;

        navigationView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer_layout);
        hamburger_fab = findViewById(R.id.hamburger_fab);
        hamburger_fab.setOnClickListener(this);

        if (!isRestored) {
            openHomeFragment();
        }

        setUpHeader();
    }

    private void setUpHeader() {
        View headerView = navigationView.getHeaderView(0);
        TextView nameTv = headerView.findViewById(R.id.nav_header_name);
        TextView emailTv = headerView.findViewById(R.id.nav_header_email);
        imageView = headerView.findViewById(R.id.nav_header_imageView);
        trackingTv = headerView.findViewById(R.id.nav_header_tracked_number);
        trackingYouTv = headerView.findViewById(R.id.nav_header_tracking_you_number);

        nameTv.setText(firebaseUser.getDisplayName());
        emailTv.setText(firebaseUser.getEmail());

        if (firebaseUser.getPhotoUrl() != null) {
            setUpNavIcon(firebaseUser.getPhotoUrl().toString());
        }

        navListeners();
        initPlaceSdk();
    }


    private void navListeners() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                MenuItem checkedItem = navigationView.getCheckedItem();
                if (checkedItem != null && checkedItem.getItemId() == menuItem.getItemId())
                    return false;

                Fragment fragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        storeHomeFragment = new HomeFragment();
                        fragment = storeHomeFragment;
                        break;

                    case R.id.nav_requests:
                        fragment = new RequestFragment();
                        break;

                    case R.id.nav_settings:
                        fragment = new SettingsFragment();
                        break;

                    case R.id.nav_log_out:
                        logOut();
                        break;
                }

                activeHomeFragmentState = ActiveHomeFragmentState.NONE;
                bottomSheetState = ActiveHomeBottomSheetState.NONE;


                if (fragment != null) {
                    setUpFragment(fragment);
                    decideHamburgerVisibility(fragment);
                }

                hideDrawer();
                return true;
            }
        });
    }

    private void decideHamburgerVisibility(Fragment fragment) {
        if (!(fragment instanceof HomeFragment)) {
            hideHamburgerFab();
        } else {
            showHamburgerFab();
        }
    }


    private void setUpFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();
    }

    private void openHomeFragment() {
        if (storeHomeFragment == null) {
            storeHomeFragment = new HomeFragment();
        }
        setUpFragment(storeHomeFragment);
        navigationView.setCheckedItem(R.id.nav_home);
    }


    /*******************   PlaceSdk Methods   ********************/

    private void initPlaceSdk() {
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getResources().getString(R.string.google_api_key));
        }

        PlacesClient placesClient = Places.createClient(this);

        autocomplete_container = findViewById(R.id.autocomplete_container);
        setUpSupportMap();
    }

    private void setUpSupportMap() {
        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
        autocompleteFragment.setCountry("NG"); //NIGERIA


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                showToast("Place: " + place.getName() + ", " + place.getId());
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                hideSupportMap();
            }

            @Override
            public void onError(@NonNull Status status) {
                showToast(status.getStatusMessage());
                Log.i(TAG, "An error occurred: " + status);
                hideSupportMap();
            }
        });
    }

    public void displaySupportMap() {
        autocompleteFragment.setText("");
        autocomplete_container.setVisibility(View.VISIBLE);
    }

    public void hideSupportMap() {
        autocomplete_container.setVisibility(View.GONE);
    }

    public void openAutocompleteIntent() {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry("NG") //NIGERIA //TODO country should be gotten dynamically
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    private void fetchPlace() {
        /*List<Place>

        FetchPlaceRequest fetchPlaceRequest= FetchPlaceRequest.builder()
        placesClient.fetchPlace()*/
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.hamburger_fab:
                openDrawer();
                break;
        }

    }


    /*******************   Permission Methods Abstracted to another class  ********************/

    public void requestLocationPermission() {
        mapHelper.requestLocationPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                    begin();
                } else {
                    Prefs.putBoolean(KEY_PERMISSION_PROMPTED_AFTER_FIRST_TIME, true);
                    requestLocationPermission();
                }
            }
        }
    }

    /*******************   isGpsEnabled Methods   ********************/

    public boolean isGpsEnabled() {
        return mapHelper.isGpsEnabled();
    }


    /*******************   onActivityResult   ********************/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.w(TAG, "onActivityResult: Called");
        if (requestCode == REQUEST_ENABLE_GPS && resultCode == RESULT_OK) {
            Log.w(TAG, "REQUEST_ENABLE_GPS: Called");

            if (locationPermissionGranted) {
                begin();
            } else {
                requestLocationPermission();
            }
            Log.w(TAG, "onActivityResult: called for REQUEST_ENABLE_GPS");

        } else if (requestCode == PERMISSIONS_REQUEST_NEVER_OPEN_AGAIN) {
            Log.w(TAG, "onActivityResult: called for PERMISSIONS_REQUEST_NEVER_ASK_AGAIN");

            if (resultCode == RESULT_OK) {
                Log.w(TAG, "onActivityResult: called for PERMISSIONS_REQUEST_NEVER_ASK_AGAIN RESULT_OK");

                locationPermissionGranted = true;
                begin();
            }

        } else if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
                showToast("ID: " + place.getId() + "address:" + place.getAddress() + "Name:" + place.getName() + " latlong: " + place.getLatLng());
                String address = place.getAddress();
                // do query with address

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                showToast("Error: " + status.getStatusMessage());
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                showToast("Cancelled");
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    /*******************   Others   ********************/

    public void logOut() {
        if (auth != null) {
            auth.signOut();
            goToLogin();
            finish();
        }
    }

    private void hideDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }


    public void hideHamburgerFab() {
        hamburger_fab.hide();
    }

    public void showHamburgerFab() {
        hamburger_fab.show();
    }


    /*******************   Lifecycle   ********************/


    @Override
    protected void onStart() {
        super.onStart();
        observeAppUserDetails();
    }

    private void observeAppUserDetails() {
        observeUser();
        //observePlace();
    }

    private void observeUser() {
        ListenerRegistration registration = userRef.document(firebaseUser.getUid()).addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d(TAG, "onEvent observeAppUserDetails: " + e.getMessage());
                    return;
                }

                if (documentSnapshot != null) {
                    User user = documentSnapshot.toObject(User.class);
                    App.setAppUser(user);

                    setUpNavTrackingDetails(user);
                }
            }
        });
        listenerRegistrations.add(registration);

    }

    private void observePlace() {
        placeRef.whereEqualTo("ownerId", firebaseUser.getUid()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d(TAG, "onEvent observeAppUserDetails: " + e.getMessage());
                    return;
                }

                List<TrackedPlace> trackedPlaceList = new ArrayList<>();


                if (queryDocumentSnapshots != null) {
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        TrackedPlace trackedPlace = documentSnapshot.toObject(TrackedPlace.class);
                        trackedPlace.setId(documentSnapshot.getId());
                        trackedPlaceList.add(trackedPlace);
                    }
                }

                showToast("You have " + trackedPlaceList.size() + " Tracked Places");
                Log.d(TAG, "onEvent: trackedPlaceList " + trackedPlaceList.size());
            }
        });
    }

    private void setUpNavTrackingDetails(User user) {
        if (navigationView == null || user == null) return;

        if (user.getTracking() != null) {
            trackingTv.setText(String.valueOf(user.getTracking().size()));
        }
        if (user.getTrackedBy() != null) {
            trackingYouTv.setText(String.valueOf(user.getTrackedBy().size()));
        }
        if (user.getImageUrl() != null) {
            setUpNavIcon(user.getImageUrl());
        }
    }

    public void setUpNavIcon(String photoUrl) {
        Glide.with(this).load(photoUrl).into(imageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w(TAG, "onResume: called");


        //TODO start something - updateLocationUI();

        //TODO this method should be place somewhere else and properly used as a checking method
        checkNetworkState();
    }

    @Override
    protected void onStop() {
        super.onStop();
        for (ListenerRegistration registration : listenerRegistrations) {
            registration.remove();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            hideDrawer();
        } else if (bottomSheetState != ActiveHomeBottomSheetState.NONE) {
            closeHomeBottomSheet();
        } else if (activeHomeFragmentState == ActiveHomeFragmentState.LIST) {
            if (storeHomeFragment != null) {
                storeHomeFragment.openMapsFragment();
            }
        } else if (navigationView.getCheckedItem().getItemId() != R.id.nav_home) {
            openHomeFragment();
        } else {
            showExitAlert();
        }
    }


    private void closeHomeBottomSheet() {
        if (storeHomeFragment == null) return;

        Log.d(TAG, "closeHomeBottomSheet: ActiveHomeBottomSheetState = " + bottomSheetState.name());
        if (bottomSheetState == ActiveHomeBottomSheetState.LOCATION_B_SHEET) {
            storeHomeFragment.hideLocationBottomSheet();
        } else if (bottomSheetState == ActiveHomeBottomSheetState.EXTRA_FAB_B_SHEET) {
            storeHomeFragment.hideExtrafabBottomSheet();
        } else {
            storeHomeFragment.hideWeatherDetailsBottomSheet();
        }
    }

    private void showExitAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.logo)
                .setMessage("Are you sure you want to exit WeatherUpFront")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.super.onBackPressed();
                        //dialogInterface.dismiss();
                    }
                }).create().show();

    }


}

