package com.ehmaugbogo.weatherupfront.views.userDetails;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.api.Response.WeatherResponse;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

import static com.ehmaugbogo.weatherupfront.utils.App.weatherResponses;

public class UserDetailsActivity extends BaseActivity {
    private static final String TAG = "UserDetailsActivity";
    public static final String USER_DETAILS="com.ehmaugbogo.weatherupfront_USER_DETAILS";
    WeatherResponse weatherResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapseToolBar = findViewById(R.id.Collapse_toolbar);

        ImageView userImg = findViewById(R.id.user_details_user_img);
        ImageView weatherImg = findViewById(R.id.user_details_weather_img);
        TextView weatherDesc = findViewById(R.id.user_details_weather_desc);
        TextView weatherDegree = findViewById(R.id.user_details_waether_degree);
        TextView weatherLocation = findViewById(R.id.user_details_location);


        if(getIntent()!=null){
            User currentUser = getIntent().getParcelableExtra(USER_DETAILS);

            if (currentUser==null){
                showToast("Error retrieving this user");
                return;
            }

            collapseToolBar.setTitle(currentUser.getDisplayName());
            if (currentUser.getImageUrl() != null){
                Glide.with(this).load(currentUser.getImageUrl()).into(userImg);
            } else {
                userImg.setImageResource(R.drawable.head);
            }


            if(getWeatherResponses()==null){
                showToast("Error retrieving weather data set");
                return;
            }


            Log.d(TAG, "onCreate: weatherResponse List Size"+weatherResponses.size()+" UserName "+currentUser.getDisplayName());


            for(WeatherResponse weatherResponse:weatherResponses){
                if(weatherResponse.getStoredUser().getUid().equals(currentUser.getUid())){
                    this.weatherResponse=weatherResponse;
                }
            }

            if(weatherResponse==null){
                showToast("Error retrieving weather data for "+currentUser.getDisplayName());
                weatherDegree.setText(String.format("Error retrieving weather data for %s", currentUser.getDisplayName()));
                weatherDesc.setText(R.string.try_again);
                return;
            }




            WeatherResponse.Weather weather = weatherResponse.getWeather().get(0);
            String weatherIconUrl= "http://openweathermap.org/img/w/" + weather.getIcon() + ".png";
            Glide.with(this).load(weatherIconUrl).into(weatherImg);

            weatherLocation.setText(String.format("%s - %s", weatherResponse.getName(), weatherResponse.getSys().getCountry()));
            weatherDesc.setText(weather.getDescription());
            weatherDegree.setText(String.valueOf(weatherResponse.getMain().getTemp() + "°F"));


        }

    }

    private List<WeatherResponse> getWeatherResponses() {
        return weatherResponses;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
