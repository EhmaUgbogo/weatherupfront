package com.ehmaugbogo.weatherupfront.views.signup_signin.forgot_password;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.BaseFragment;

public class MailSentFragment extends BaseFragment {

    View root;
    TextView not_mail;
    public MailSentFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        root = inflater.inflate(R.layout.fragment_mail_sent, container, false);

        not_mail = root.findViewById(R.id.not_mail);

        not_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(getContext(), ForgotPasswordActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
        return root;
    }
}
