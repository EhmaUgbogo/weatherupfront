package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.List;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackedPlace;
import com.ehmaugbogo.weatherupfront.views.BaseFragment;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.adapters.TrackedPlacesAdapter;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class TrackedPlacesFragment extends BaseFragment {
    private static final String TAG = "TrackedPlacesFragment";
    private View nothingFoundView;
    private ListenerRegistration listenerRegistration;
    private TrackedPlacesAdapter trackedPlacesAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tracked_places, container, false);
        setup(view);
        return view;
    }

    private void setup(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.places_recycler);
        nothingFoundView = view.findViewById(R.id.nothing_found);
        trackedPlacesAdapter = new TrackedPlacesAdapter();
        recyclerView.setAdapter(trackedPlacesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onStart() {
        super.onStart();
        getTrackedPlaces();
    }

    private void getTrackedPlaces() {
        listenerRegistration = placeRef.whereEqualTo("ownerId", appUser().getUid())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "onEvent: " + e.getMessage());
                            return;
                        }

                        List<TrackedPlace> trackedPlaceList = new ArrayList<>();

                        if (queryDocumentSnapshots != null) {
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                TrackedPlace trackedPlace = documentSnapshot.toObject(TrackedPlace.class);
                                trackedPlace.setId(documentSnapshot.getId());
                                trackedPlaceList.add(trackedPlace);
                            }
                        }

                        decideDisplayView(trackedPlaceList);

                        trackedPlacesAdapter.summitTrackPlaces(trackedPlaceList);
                        Log.d(TAG, "onEvent: trackedPlaceList " + trackedPlaceList.size());
                    }
                });
    }

    private void decideDisplayView(List<TrackedPlace> trackedPlaceList) {
        if (trackedPlaceList.size() > 0){
            hideEmptyView();
        } else {
            showEmptyView();
        }
    }

    private void showEmptyView() {
        nothingFoundView.setVisibility(View.VISIBLE);
    }

    private void hideEmptyView() {
        nothingFoundView.setVisibility(View.GONE);
    }


    @Override
    public void onStop() {
        super.onStop();
        listenerRegistration.remove();
    }

}
