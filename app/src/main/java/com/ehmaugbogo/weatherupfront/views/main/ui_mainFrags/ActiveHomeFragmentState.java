package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags;

public enum ActiveHomeFragmentState {
    MAP,
    LIST,
    NONE;
}
