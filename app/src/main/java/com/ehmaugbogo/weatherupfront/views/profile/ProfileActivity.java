package com.ehmaugbogo.weatherupfront.views.profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.ehmaugbogo.weatherupfront.views.profile.editProfile.EditProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseActivity {
    private static final String TAG = "ProfileActivity";
    public static final String PROFILE_USER="com.ehmaugbogo.weatherupfront_PROFILE_USER";
    private CircleImageView userProfileImage;
    private ImageView mBackArrow, userProfileImage_background;
    private TextView mUserProfileName, mUserProfileEmail, mUserProfileEmail2,mUserName,
             mUserProfile_phone, mUserProfile_location;
    private Button mEdit;

    //Upload
    private ProgressBar mProgressBar;
    private StorageTask mUploadTask;

    private boolean changePhoto;
    private User profileUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        try {
            initView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!changePhoto){
                    showToast("Click again to change display picture");
                    changePhoto=true;
                    return;
                }

                if (mUploadTask != null && mUploadTask.isInProgress()){
                    showToast("Upload in Progress");
                }else{
                    selectPhoto();
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void initView() throws Exception {
        setWindowFlag(this);
        if(getIntent()==null){
            throw new Exception("Please pass a profileUser intent to this Activity");
        }

        mBackArrow = findViewById(R.id.userProfile_bck_arrow);
        mUserProfileName = findViewById(R.id.userProfileName);
        mUserProfileEmail = findViewById(R.id.userProfileEmail);


        mUserName = findViewById(R.id.userName_tv);
        mUserProfileEmail2 = findViewById(R.id.userProfile_email);
        mUserProfile_phone = findViewById(R.id.userProfile_phone);
        mUserProfile_location = findViewById(R.id.userProfile_location);
        mEdit = findViewById(R.id.edit_btn);
        userProfileImage = findViewById(R.id.profile_photo_display_image);
        userProfileImage_background = findViewById(R.id.userProfileImage_background);

        mProgressBar = findViewById(R.id.profile_photo_display_progress);
        mProgressBar.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(this,R.color.colorPrimaryLight)));


        profileUser = getIntent().getParcelableExtra(PROFILE_USER);
        startSetup(profileUser);


        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), EditProfileActivity.class));
            }
        });

        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        userProfileImage_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ProfilePhotoActivity.class);
                intent.putExtra(ProfilePhotoActivity.PROFILE_USER,profileUser.getImageUrl());
                startActivity(intent);
            }
        });
    }

    private void startSetup(User profileUser) {
        if(profileUser !=null) {
            setViewData(profileUser);
            if(!profileUser.getUid().equals(appUser().getUid())){
                mEdit.setVisibility(View.GONE);
                userProfileImage.setVisibility(View.INVISIBLE);
            }
        }
    }


    private void setViewData(User user) {
        if (user.getImageUrl() != null){
            displayUserPhoto(user.getImageUrl());
        } else {
            userProfileImage.setImageResource(R.drawable.head);
        }

        mUserProfileName.setText(user.getDisplayName());
        mUserProfileEmail.setText(user.getEmail());

        mUserName.setText(user.getDisplayName());
        mUserProfile_phone.setText(user.getPhoneNo());
        mUserProfileEmail2.setText(user.getEmail());
        mUserProfile_location.setText(user.getAddress());


        if(user.getPhoneNo()==null){
            mUserProfile_phone.setText(R.string.not_added_yet);
        }

        if(user.getAddress()==null){
            mUserProfile_location.setText(R.string.not_added_yet);
        }
    }

    private void displayUserPhoto(String url){
        Glide.with(this).load(url).centerCrop()
                .placeholder(R.drawable.head).into(userProfileImage);

        Glide.with(this).load(url).centerCrop()
                .placeholder(R.drawable.head).into(userProfileImage_background);
    }



    private void selectPhoto(){
        CropImage.activity()
                .setAspectRatio(1,1)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK){
                uploadFile(result.getUri());
            }else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                showToast("Error retrieving photo");
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(profileUser !=null&&profileUser.getUid().equals(appUser().getUid())) {
            startSetup(appUser());
        }

    }

    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadFile(Uri imageUri){
        mProgressBar.setVisibility(View.VISIBLE);
        displayUserPhoto(imageUri.toString());

        StorageReference imageReference = storageRef.child(appUser().getUid());
        mUploadTask = imageReference.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                final String download_uri = uri.toString();
                                appUser().setImageUrl(download_uri);
                                userRef.document(firebaseUser.getUid()).update("imageUrl", download_uri)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){

                                                    Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            changePhoto=false;
                                                            mProgressBar.setVisibility(View.GONE);
                                                            mProgressBar.setProgress(0);
                                                        }
                                                    }, 500);

                                                    Log.d(TAG, "onComplete: ProfilePhoto saved to firestore database");
                                                } else {
                                                    showToast("Unsuccessful");
                                                }
                                            }
                                        });

                            }
                        });


                        showToast("Profile photo change successful");
                        Log.d(TAG, "onComplete: ProfilePhoto saved to Storage database");


                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                        int progressInt=(int)progress;
                        mProgressBar.setProgress((int)progress);
                        if(progressInt<99){
                            mUserProfileEmail.setText(String.format("%d%%", progressInt));
                        } else {
                            mUserProfileEmail.setText(appUser().getEmail());
                        }
                    }
                });

    }

    public static void setWindowFlag(Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }



}
