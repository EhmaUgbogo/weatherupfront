package com.ehmaugbogo.weatherupfront.views.signup_signin.forgot_password;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends BaseActivity {
    private Button confirm_btn;
    private EditText user_email;
    private ProgressBar forgot_bar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        confirm_btn = findViewById(R.id.forgot_confirm);
        user_email = findViewById(R.id.forgot_email);
        forgot_bar = findViewById(R.id.forgot_bar);

        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String u_email = user_email.getText().toString();
                if(TextUtils.isEmpty(u_email)){
                    user_email.setError("Please Enter your email");
                    showToast("Please Enter a valid email address");
                }else{
                    forgot_bar.setVisibility(View.VISIBLE);
                    confirm_btn.setClickable(false);
                    sendForgotMail(u_email);
                }
            }
        });
    }


    private void sendForgotMail(String email_address) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        Fragment fragment = new MailSentFragment() ;
        auth.sendPasswordResetEmail(email_address).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    forgot_bar.setVisibility(View.INVISIBLE);
                    closeKeyboard();
                    getSupportFragmentManager().beginTransaction().replace(R.id.forgot_frame, fragment).commit();
                    showToast("Please check your mail for next steps");
                }else{
                    forgot_bar.setVisibility(View.INVISIBLE);
                    confirm_btn.setClickable(true);
                    showToast("Please try again later");
                }
            }
        });
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null :
                getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
