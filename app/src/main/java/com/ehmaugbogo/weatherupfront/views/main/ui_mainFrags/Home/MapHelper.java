package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

import com.ehmaugbogo.weatherupfront.views.main.MainActivity;
import com.pixplicity.easyprefs.library.Prefs;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MapHelper {
    private static final String TAG = "MapHelper";

    private Context context;
    private MainActivity mainActivity;

    private String[] permissions;
    private boolean dialogIsOpen=false;


    public MapHelper(Context context) {
        this.context = context;
        mainActivity =(MainActivity)context;
        Log.w(TAG, "MapHelper: Created");

    }


    public void requestLocationPermission() {
        Log.w(TAG, "requestLocationPermission: Called");
        permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

        if (ContextCompat.checkSelfPermission(context.getApplicationContext(), permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            MainActivity.locationPermissionGranted = true;
            mainActivity.begin();

        } else {
            boolean permissionState = ActivityCompat.shouldShowRequestPermissionRationale(mainActivity, permissions[0]);
            /*This method returns true if the user has previously denied the request. But if the user has selected “Dont’ ask again”
            this method would always false . It also returns false if we are prompting for permission for the first time.*/

            Log.w(TAG, "requestLocationPermission: permissionState Called");
            if (permissionState&&!dialogIsOpen) { //request denied RETURNS true
                Log.w(TAG, "request denied: openPermissionRationale Called");
                openPermissionRationale();
            } else { //“Don't ask again”| permission prompted for the first time RETURNS false
                boolean afterFirstTimeRequest = Prefs.getBoolean(MainActivity.KEY_PERMISSION_PROMPTED_AFTER_FIRST_TIME, false);
                if (afterFirstTimeRequest) { //“Don't ask again”
                    if(!dialogIsOpen) {
                        Log.w(TAG, "Don't ask again: permissionState Called");
                        openNeverAskAgainDialog();
                    }
                } else { //permission prompted for the first time
                    Log.w(TAG, "first time: openPermissionRationale Called");
                    openPermissionRationale();
                }
            }
        }
    }


    private void openPermissionRationale() {
        dialogIsOpen=true;
        Log.w(TAG, "openPermissionRationale dialogIsOpen = true: Called");
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Location permission is needed so we can access your location and provide weather feedback")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogIsOpen=false;
                        ActivityCompat.requestPermissions(mainActivity, permissions,
                                MainActivity.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                        Log.w(TAG, "openPermissionRationale onClick: dialogIsOpen = false: Called");
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void openNeverAskAgainDialog() {
        dialogIsOpen=true;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Permissions Needed!")
                .setMessage("WeatherUpFront may not work correctly without the requested permissions. Please enable the permissions through "
                        + "Settings screen -> Select Permissions -> Enable permission");
        builder.setCancelable(false);
        builder.setPositiveButton("Permit Manually", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogIsOpen=false;
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                mainActivity.startActivityForResult(intent, MainActivity.PERMISSIONS_REQUEST_NEVER_OPEN_AGAIN);
                dialog.dismiss();
            }
        });
        builder.show();
    }


    /*******************   isGpsEnabled Methods   ********************/

    public boolean isGpsEnabled() {
        Log.w(TAG, "isGpsEnabled: Called");
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            openGpsAlertDialog();
            Log.w(TAG, "isGpsEnabled: False");
            return false;
        }

        Log.w(TAG, "isGpsEnabled: True");
        return true;
    }

    private void openGpsAlertDialog() {
        Log.w(TAG, "openGpsAlertDialog: Called");
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Your GPS location is currently turned off. Please turn on to access all features of WeatherUpFront")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent gpsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mainActivity.startActivityForResult(gpsIntent, MainActivity.REQUEST_ENABLE_GPS);

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }



}
