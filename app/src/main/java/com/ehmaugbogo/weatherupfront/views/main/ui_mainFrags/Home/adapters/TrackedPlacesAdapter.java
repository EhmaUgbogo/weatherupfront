package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackedPlace;

import java.util.ArrayList;
import java.util.List;

public class TrackedPlacesAdapter extends RecyclerView.Adapter<TrackedPlacesAdapter.ViewHolder> {
    private List<TrackedPlace> placeList=new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_details, parent, false);
        return new TrackedPlacesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TrackedPlacesAdapter.ViewHolder holder, int position) {

        TrackedPlace item = placeList.get(position);
        holder.categoryIcon.setImageResource(R.drawable.ic_work_black);
        holder.name.setText(item.getCustomName());
        holder.temprature.setText(item.getWeatherTemp());

    }

    @Override
    public int getItemCount() {
        return placeList.size();
    }

    public void summitTrackPlaces(List<TrackedPlace> trackedPlaceList) {
        placeList=trackedPlaceList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView categoryIcon;
        private TextView name;
        private TextView temprature;
        private LottieAnimationView weatherImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryIcon = itemView.findViewById(R.id.item_details_weather_category_icon);
            name = itemView.findViewById(R.id.item_details_weather_name);
            temprature = itemView.findViewById(R.id.item_details_weather_temprature);
            weatherImage = itemView.findViewById(R.id.bottom_sh_weather_weather_image);

        }
    }

 }