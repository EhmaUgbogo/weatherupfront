package com.ehmaugbogo.weatherupfront.DataStoreArchitecture.ViewModel;

import android.app.Application;

import com.ehmaugbogo.weatherupfront.DataStoreArchitecture.Repository;
import com.ehmaugbogo.weatherupfront.api.WeatherAPI;
import com.ehmaugbogo.weatherupfront.models.TrackRequest;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper.WeatherClusterItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class WeatherViewModel extends AndroidViewModel {
    private static final String TAG = "WeatherViewModel";
    private final Repository repository;
    private MutableLiveData<List<TrackRequest>> sentRequestData =new MutableLiveData<>();
    private MutableLiveData<List<TrackRequest>> receivedRequestData =new MutableLiveData<>();
    private MutableLiveData<WeatherClusterItem> clusterItem =new MutableLiveData<>();


    public WeatherViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
    }


    public WeatherAPI getWeatherAPI() {
        return repository.getWeatherAPI();
    }


    public LiveData<WeatherClusterItem> getClusterItem() {
        return clusterItem;
    }

    public void setClusterItem(WeatherClusterItem clusterItem) {
        this.clusterItem.setValue(clusterItem);
    }


    //**************************  SentRequestData  ***************************************/

    public LiveData<List<TrackRequest>> getSentRequestData() {
        return sentRequestData;
    }

    public void setSentRequestData(List<TrackRequest> trackRequestList) {
        sentRequestData.setValue(trackRequestList);
    }

    public void setSentRequestDataPost(List<TrackRequest> trackRequestList) {
        sentRequestData.postValue(trackRequestList);
    }


    //**************************  receivedRequestData  ***************************************/

    public LiveData<List<TrackRequest>> getReceivedRequestData() {
        return receivedRequestData;
    }

    public void setReceivedRequestData(List<TrackRequest> trackRequestList) {
        receivedRequestData.setValue(trackRequestList);
    }

    public void setReceivedRequestDataPost(List<TrackRequest> trackRequestList) {
        receivedRequestData.postValue(trackRequestList);
    }



}
