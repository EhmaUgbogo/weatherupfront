package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.mapHelper;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.User;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class WeatherInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private View infoView;
    private String title, description;


    public WeatherInfoWindowAdapter(Context context, String title, String description) {
        init(context, title, description);

    }

    public WeatherInfoWindowAdapter(Context context, User user) {
        title=user.getDisplayName();
        description="Info window description hardcoded";
        init(context, title, description);

    }

    private void init(Context context, String title, String description) {
        this.title = title;
        this.description = description;

        infoView = View.inflate(context, R.layout.info_window, null);
        ImageView imageView = infoView.findViewById(R.id.info_imageView);
        TextView nameText = infoView.findViewById(R.id.info_title_textView);
        TextView descriptionText = infoView.findViewById(R.id.info_description_textView);

        nameText.setText(title);
        descriptionText.setText(description);
        //Glide.with(context).load(user.getImageUrl()).into(imageView);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return infoView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        marker.setTitle(title);
        marker.setSnippet(description);
        return infoView;
    }
}
