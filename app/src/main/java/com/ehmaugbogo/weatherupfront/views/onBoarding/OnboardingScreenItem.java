package com.ehmaugbogo.weatherupfront.views.onBoarding;

public class OnboardingScreenItem {

    String Description;
    int ScreenImg;

    public OnboardingScreenItem(String description, int screenImg) {
        Description = description;
        ScreenImg = screenImg;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setScreenImg(int screenImg) {
        ScreenImg = screenImg;
    }

    public String getDescription() {
        return Description;
    }

    public int getScreenImg() {
        return ScreenImg;
    }
}
