package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.TrackRequest;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Request.OnRequestClickedCallBack;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class ReceivedRequestAdapter extends RecyclerView.Adapter<ReceivedRequestAdapter.ViewHolder> {
    private final Context context;
    private List<TrackRequest> trackRequests=new ArrayList<>();

    private OnRequestClickedCallBack onRequestClicked;


    public ReceivedRequestAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request, parent, false);
        return new ReceivedRequestAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReceivedRequestAdapter.ViewHolder holder, int position) {
        TrackRequest request = trackRequests.get(position);
        holder.mName.setText(request.getSender().getDisplayName());
        holder.mEmail.setText(request.getSender().getEmail());
    }

    @Override
    public int getItemCount() {
        return trackRequests.size();
    }

    public void summitTrackRequests(List<TrackRequest> trackRequests) {
        this.trackRequests = trackRequests;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mName;
        private TextView mEmail;
        private final ImageButton deleteBtn;

        private ViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.request_name);
            mEmail = itemView.findViewById(R.id.request_email);
            deleteBtn = itemView.findViewById(R.id.request_imageBtn);
            deleteBtn.setVisibility(View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getAdapterPosition()!=RecyclerView.NO_POSITION){
                        onRequestClicked.onRequestClicked(trackRequests.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    protected void showToast(String message) {
        Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }


    //******************** OnRequestClickedCallBack *****************************/


    public void setOnRequestClicked(OnRequestClickedCallBack onRequestClicked) {
        this.onRequestClicked = onRequestClicked;
    }



}