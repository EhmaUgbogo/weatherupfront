package com.ehmaugbogo.weatherupfront.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.ehmaugbogo.weatherupfront.views.BaseFragment;
import com.ehmaugbogo.weatherupfront.views.login.LoginActivity;
import com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.ActiveHomeBottomSheetState;
import com.ehmaugbogo.weatherupfront.views.register.RegisterActivity;
import com.ehmaugbogo.weatherupfront.views.signup_signin.forgot_password.ForgotPasswordActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

public class HomeBaseFragment extends BaseFragment {
    protected String uid;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uid = firebaseUser.getUid();
    }

    protected MainActivity rootActivity(){
        return (MainActivity) getActivity();
    }

    protected void setMainBottomSheetState(ActiveHomeBottomSheetState state){
        rootActivity().bottomSheetState= state;
    }

    protected void showCustomToast(String message) {
        View view = getLayoutInflater().inflate(R.layout.toast, getActivity().findViewById(R.id.toast_root));
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_HORIZONTAL,0,40);
        toast.setView(view);

        TextView textView=view.findViewById(R.id.toast_textView);
        ImageView imageView=view.findViewById(R.id.toast_image);
        textView.setText(message);

        toast.show();

    }

}

