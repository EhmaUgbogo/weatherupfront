package com.ehmaugbogo.weatherupfront.DataStoreArchitecture;

import android.content.Context;

import com.ehmaugbogo.weatherupfront.api.RetrofitClient;
import com.ehmaugbogo.weatherupfront.api.WeatherAPI;

public class Repository {
    private final WeatherAPI weatherAPI;
    Context context; //Will be useful later for future additions

    public Repository(Context context) {
        this.context = context;
        weatherAPI = RetrofitClient.getInstance().create(WeatherAPI.class);
    }

    public WeatherAPI getWeatherAPI() {
        return weatherAPI;
    }
}
