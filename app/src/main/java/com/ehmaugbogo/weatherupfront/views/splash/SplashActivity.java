package com.ehmaugbogo.weatherupfront.views.splash;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.ehmaugbogo.weatherupfront.views.login.LoginActivity;
import com.ehmaugbogo.weatherupfront.views.onBoarding.OnboardingActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import static com.ehmaugbogo.weatherupfront.utils.Constants.COLLECTION_USERS;

public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";
    private final String APP_OPEN_FIRST_TIME="com.ehmaugbogo.weatherupfront_APP_OPEN_FIRST_TIME";
    private boolean firstTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }


        ImageView wuf_logo = findViewById(R.id.logo_image);
        Animation heartBeatAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        wuf_logo.setAnimation(heartBeatAnim);


        firstTime = Prefs.getBoolean(APP_OPEN_FIRST_TIME, true);

        if(!firstTime&&firebaseUser!=null){
            retrieveAppUser();
        }

        navigateTo();

    }

    private void navigateTo() {
        new Handler().postDelayed(() -> {
            if (firstTime) {
                Prefs.putBoolean(APP_OPEN_FIRST_TIME,false);
                //Goto onBoarding Screen
                Intent onboarding = new Intent(getApplicationContext(), OnboardingActivity.class);
                startActivity(onboarding);
                finish();
            } else {
                //Should goto MainActivity
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        },3000);
    }

    private void retrieveAppUser(){
        if(App.getAppUser()==null){
            userRef.document(firebaseUser.getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()&&task.getResult()!=null){
                        User user = task.getResult().toObject(User.class);
                        if (user != null) {
                            user.setUid(task.getResult().getId());
                            App.setAppUser(user);
                        } else {
                            Log.w(TAG, "retrieveAppUser failed: Database details must have been deleted" +
                                    "\nregister new account or delete user account & register with same details");
                        }
                    } else {
                        Log.w(TAG, "Error "+ Objects.requireNonNull(task.getException()).getMessage());
                    }
                }
            });
        }
    }


}
