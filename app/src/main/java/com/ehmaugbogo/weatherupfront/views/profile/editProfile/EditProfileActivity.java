package com.ehmaugbogo.weatherupfront.views.profile.editProfile;

import androidx.annotation.NonNull;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.SetOptions;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends BaseActivity {

    private static final String TAG = "EditProfileActivity";
    private CircleImageView mImageView;
    private ImageView mBackArrow;
    private TextView mUserProfileName, mUserProfileEmail, mSaveUserProfile;
    private EditText mUserFirstName, mUserLastName, mUserPhone, mUserLocation;
    private ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initView();
    }

    private void initView(){
        mBackArrow = findViewById(R.id.editProfileBackArrow);
        mImageView = findViewById(R.id.userEditProfileImage);
        mUserProfileName = findViewById(R.id.editProfile_Name);
        mUserProfileEmail = findViewById(R.id.editProfile_Email);
        mUserFirstName = findViewById(R.id.edit_profile_firstname_edtxt);
        mUserLastName = findViewById(R.id.edit_profile_lastname_edtxt);
        mUserPhone = findViewById(R.id.edit_profile_phoneNumber_edtxt);
        mUserLocation = findViewById(R.id.edit_profile_location_edtxt);
        mSaveUserProfile = findViewById(R.id.saveUserProfile);
        mProgressBar = findViewById(R.id.editProfile_progress);

       showUserDetails();
       save();
       back();

    }

    private void showUserDetails(){
        if (appUser() != null) {
            mUserProfileName.setText(appUser().getDisplayName());
            mUserPhone.setText(appUser().getPhoneNo());
            mUserProfileEmail.setText(appUser().getEmail());
            mUserLocation.setText(appUser().getAddress());
            mUserFirstName.setText(appUser().getFirstName());
            mUserLastName.setText(appUser().getLastName());
            Glide.with(this).load(appUser().getImageUrl()).into(mImageView);
        }
    }

    private void save(){
        mSaveUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserProfile();
            }
        });
    }

    private void back(){
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void saveUserProfile(){
        if (mUserFirstName.getText().toString().isEmpty()||
                mUserLastName.getText().toString().isEmpty()||
                mUserPhone.getText().toString().isEmpty()||
                mUserLocation.getText().toString().isEmpty()){
            Toast.makeText(this,"All fields are required",Toast.LENGTH_SHORT).show();
        }else {
            appUser().setFirstName(mUserFirstName.getText().toString().trim());
            appUser().setLastName(mUserLastName.getText().toString().trim());
            appUser().setPhoneNo(mUserPhone.getText().toString().trim());
            appUser().setDisplayName(appUser().getFirstName()+" "+appUser().getLastName());
            appUser().setAddress(mUserLocation.getText().toString().trim());
            userRef.document(firebaseUser.getUid()).set(appUser(), SetOptions.merge())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                showToast("Profile updated successfully");
                                updateUserDetails();
                                finish();
                                mProgressBar.setVisibility(View.GONE);
                            } else {
                                showToast("Profile update unsuccessful");
                            }
                        }
                    });

        }
    }

    private void updateUserDetails() {
        Log.w(TAG, "updateUserDetails: Called");
        showToast("Please Wait");
        //Store User Image then...

        UserProfileChangeRequest changeRequest=new UserProfileChangeRequest.Builder()
                .setDisplayName(appUser().getDisplayName()).build();
        firebaseUser.updateProfile(changeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "onComplete: Profile Updated");
                } else {
                    Log.d(TAG,"Profile Update unsuccessful... Retrying");
                    updateUserDetails();
                }
            }
        });

    }
}
