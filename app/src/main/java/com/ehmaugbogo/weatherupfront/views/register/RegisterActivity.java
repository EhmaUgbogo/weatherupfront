package com.ehmaugbogo.weatherupfront.views.register;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ehmaugbogo.weatherupfront.R;
import com.ehmaugbogo.weatherupfront.models.User;
import com.ehmaugbogo.weatherupfront.utils.App;
import com.ehmaugbogo.weatherupfront.views.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class RegisterActivity extends BaseActivity {

    private static final String TAG = "RegistrationActivity";
    private EditText mFirstNameEditText, mPasswordEditText, mEmailEditText, mConfirmPasswordEditText, mLastNameEditText;
    private String mFullName, mPassword, mEmail, mConfirmPassword, mFirstName, mLastName;
    private ProgressBar progressBar;
    private Button mRegisterUserBtn;
    private TextView mGoToLogin, choosePhotoTxtView;
    private ImageView photoImgView;
    private Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initView();

    }

    private void initView() {
        mFirstNameEditText = findViewById(R.id.reg_firstName_edt);
        mLastNameEditText = findViewById(R.id.reg_lastName_edt);
        mEmailEditText = findViewById(R.id.reg_email_edt);
        mPasswordEditText = findViewById(R.id.reg_password_edt);
        mConfirmPasswordEditText = findViewById(R.id.reg_passConf_edt);
        mRegisterUserBtn = findViewById(R.id.reg_register_btn);
        mGoToLogin = findViewById(R.id.reg_gotoLogin_tv);
        choosePhotoTxtView = findViewById(R.id.reg_choose_photo);
        photoImgView = findViewById(R.id.reg_icon_im);

        progressBar = findViewById(R.id.register_progressBar);

        listeners();
    }

    private void listeners() {
        mRegisterUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: attempting to register user");
                mFirstName = mFirstNameEditText.getText().toString().trim();
                mLastName = mLastNameEditText.getText().toString().trim();
                mFullName = mFirstName + " " + mLastName;
                mPassword = mPasswordEditText.getText().toString().trim();
                mEmail = mEmailEditText.getText().toString().trim();
                mConfirmPassword = mConfirmPasswordEditText.getText().toString().trim();

                if(checkNetworkState()){
                    registerUser(mEmail, mPassword);
                }

            }
        });

        mGoToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLogin();
            }
        });

        choosePhotoTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        photoImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPhoto();
            }
        });


    }

    private void registerUser(String email, String password) {
        showProgress();
        if (validateInputs()) {
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    Log.d(TAG, "Registration successful" + task.isSuccessful());
                    if (task.isSuccessful()) {

                        updateUserDetails(Objects.requireNonNull(auth.getCurrentUser()));
                        Toast.makeText(getApplicationContext(), "Successfully Registered User", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to Register User "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
                    hideProgress();
                }
            });
        } else {
            hideProgress();
        }
    }

    private void updateUserDetails(FirebaseUser currentUser) {
        Log.w(TAG, "updateUserDetails: Called");
        showToast("Please Wait");

        StorageReference imageReference = storageRef.child(currentUser.getUid());
        imageReference.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    startNewRegProcess(currentUser);
                    return;
                }

                imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        UserProfileChangeRequest changeRequest=new UserProfileChangeRequest.Builder()
                                .setDisplayName(mFullName)
                                .setPhotoUri(uri).build();
                        currentUser.updateProfile(changeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d(TAG, "onComplete: Update successful");
                                    saveUserToFireStore(currentUser,uri.toString());
                                } else {
                                    startNewRegProcess(currentUser);
                                }
                            }
                        });
                    }
                });

                Log.d(TAG, "onComplete: ProfilePhoto saved to Storage database");
            }
        });

    }


    private void saveUserToFireStore(FirebaseUser currentUser, String url) {

        User newUser=new User(mFullName,mEmail);
        newUser.setUid(currentUser.getUid());
        newUser.setFirstName(mFirstName);
        newUser.setLastName(mLastName);
        newUser.setDisplayName(mFullName);
        newUser.setDateJoined(null); //Leave exactly like this
        newUser.setImageUrl(url);

        App.setAppUser(newUser);

        userRef.document(currentUser.getUid()).set(newUser)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.w(TAG, "saveUserToFireStore: New User Saved to database");
                            goToMainActivity();
                        } else {
                            Log.w(TAG, "saveUserToFireStore Error: "+task.getException().getMessage());
                            Log.w(TAG, "saveUserToFireStore Restarting saving user");
                            saveUserToFireStore(currentUser,url);
                        }
                    }
                });
    }

    private void startNewRegProcess(FirebaseUser currentUser) {
        currentUser.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                hideProgress();
                auth.signOut();
                Log.d(TAG, "startNewRegProcess: User Deleted.");
                showToast("Error registering user. Please try again");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "OnFailureListener: "+e.toString());
                hideProgress();
            }
        });

    }


    private boolean validateInputs() {
        mFirstNameEditText.setFilters(new InputFilter[]{new ValidateFilter()});
        mLastNameEditText.setFilters(new InputFilter[]{new ValidateFilter()});

        if (!isValidEmail(mEmail)) {
            mEmailEditText.setError("Email Required");
            return false;
        }

        if (!isValidPassword(mPassword)) {
            mPasswordEditText.setError("Password of 8 characters & above Required");
            return false;
        }

        if (!isValidConfirmPassword()) {
            mConfirmPasswordEditText.setError("Confirm Password not match");
            return false;
        }

        if (imageUri==null) {
            showToast("Please select a display picture");
            return false;
        }

        return true;
    }


    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public boolean isValidPassword(String pass) {
        return pass != null && pass.length() >= 8;
    }

    public boolean isValidConfirmPassword() {
        return mPassword.equals(mConfirmPassword);
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        mRegisterUserBtn.setEnabled(false);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        mRegisterUserBtn.setEnabled(true);
    }


    //************************************************************************/

    private void selectPhoto(){
        CropImage.activity()
                .setAspectRatio(1,1)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK&&result != null){
                imageUri = result.getUri();
                displayUserPhoto(imageUri.toString());

            }else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                showToast("Error retrieving photo");
            }

        }
    }

    private void displayUserPhoto(String url){
        choosePhotoTxtView.setText(R.string.register);
        Glide.with(this).load(url).centerCrop()
                .placeholder(R.drawable.head).into(photoImgView);
    }





}
