package com.ehmaugbogo.weatherupfront.views.main.ui_mainFrags.Home.List;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ehmaugbogo.weatherupfront.R;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private int[] TAB_TITLES = new int[]{ R.string.tracked_places, R.string.tracked_friends};
    private final Context mContext;



    public SectionsPagerAdapter(@NonNull FragmentManager fm, int behavior,  Context context) {
        super(fm, behavior);
        this.mContext = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        if(position == 0) fragment = new TrackedPlacesFragment();

        else if (position == 1) fragment = new TrackedFriendsFragment();

        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return TAB_TITLES.length;
    }
}
