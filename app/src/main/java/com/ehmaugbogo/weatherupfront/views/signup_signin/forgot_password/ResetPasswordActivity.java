package com.ehmaugbogo.weatherupfront.views.signup_signin.forgot_password;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ehmaugbogo.weatherupfront.R;

public class ResetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
    }
}
